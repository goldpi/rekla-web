﻿namespace Rakla.Infra.Interface
{
    public interface IApplicationConfig
    {
        string GetAppProvider(ApplicationProviders type);
    }

    public enum ApplicationProviders
    {
        SMS,
        EMAIL,
        PAYMENTGATEWAY,
        PLAYSTORE,
        APPSTORE,
        WINDOWSSTORE,
    }
}
