﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rakla.Infra.Interface
{
    public interface ILinkStripper
    {
        char[] GetRemovingChatrates();
    }
}
