﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rakla.Infra.Interface
{
    public interface IShortMessageSender
    {
        //string User { get; }
        //string Password { get; }
        string BaseUrl { get; }
        string Parameters { get; }
        string Method { get; }
        string SuccessResponse { get; }
        bool SendMessage(string No, string Message);
        Task<bool> SendMessageAsync(string No, string Message);

    }
}
