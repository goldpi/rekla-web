﻿using Ninject;
using Rakla.Infra.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rakla.Infra.Interface
{
    public interface IGateWay
    {
        string Key { get; }
        string Salt { get; }
        string EndPoint { get; }
        string SuccesUrl { get; }
        string FailedUrl { get; }


        string MakePayment(string Txnid, string Amount, string ProductInfo, string Email, string Phone, string FirstName);
        string PaymentSuccess(dynamic Object);
        string PaymentFailed(dynamic Object);
        string PaymentCanceled(dynamic Object);
    }

    public enum PyamentGatewaySettings
    {
        Key,
        Salt,
        DevKey,
        DevSalt,
        TestUrl,
        LiveUrl,
        SuccesUrl,
        FailedUrl

    }

    public interface IGatewayConfig
    {
        string GetConfiguration(PyamentGatewaySettings config);
    }
    public class GatewaynConfig : IGatewayConfig
    {
        private ConfigReader reader;
        public GatewaynConfig(string path)
        {
            reader = new ConfigReader(path);
        }
        public string GetConfiguration(PyamentGatewaySettings config)
        {
            return reader.keyValues[config.ToString()];
        }
    }

    public static class GatewayConfigHelper
    {
        private static IGatewayConfig _config;
        private static IKernel _diResover;
        public static void Init(IKernel DIResolver)
        {
            _diResover = DIResolver;
            _config = _diResover.Get<IGatewayConfig>();
        }

        public static string Value(this PyamentGatewaySettings e)
        {
            return _config.GetConfiguration(e);
        }
    }
}
