﻿using Ninject;
using Rakla.Infra.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rakla.Infra.Helper
{
    public static class ShortMessageHelper
    {
        private static IShortMessageSender _provider;
        private static IKernel _di;
        public static void Init(IKernel DI)
        {
            _di = DI;
            _provider = _di.Get<IShortMessageSender>();
        }


        public static bool SendMessage(string No, string Message)
        {
            return _provider.SendMessage(No, Message);
        }

        public static async Task<bool> SendMessageAsync(string No, string Message)
        {
            return await _provider.SendMessageAsync(No, Message);
        }
    }
}
