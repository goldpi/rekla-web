﻿using Ninject;
using Rakla.Infra.Interface;

namespace Rakla.Infra.Helper
{
    public static class ApplicationConfigHelper
    {
        static IKernel _dependencyResolver;
        static IApplicationConfig _config;
        public static void Init(IKernel Resolver)
        {
            _dependencyResolver = Resolver;
            _config = _dependencyResolver.Get<IApplicationConfig>();
        }

        public static string GetProvider(this ApplicationProviders Type)
        {
            return _config.GetAppProvider(Type);
        }
    }
}