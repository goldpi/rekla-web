﻿using Ninject;
using Rakla.Infra.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rakla.Infra.Helper
{
    public static class LinkStripperHelper
    {
        static IKernel _di;
        static ILinkStripper _stripper;
        public static void Init(IKernel dependencyResover)
        {
            _di = dependencyResover;
            _stripper = _di.Get<ILinkStripper>();
        }

        public static string ToValidUrlString(this string val)
        {
            foreach (var str in _stripper.GetRemovingChatrates())
                val = val.Replace(str, '-');
            return val;
        }
    }
}
