﻿using Rakla.Infra.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Rakla.Infra.Concrete
{
    public class LinkStripper : ILinkStripper
    {
        ConfigReader _config;
        public LinkStripper(string paths)
        {
            _config = new ConfigReader(paths);
        }
        public char[] GetRemovingChatrates()
        {
            return HttpUtility.HtmlDecode((_config.
                Properties.
                First().
                Value as String)).
                ToArray();
        }
    }

    
}
