﻿using Rakla.Infra.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Rakla.Infra.Concrete
{
    public class ShortMessageSender : IShortMessageSender
    {
        private ConfigReader _messageProviderConfig;
        public ShortMessageSender(string path)
        {
            _messageProviderConfig = new ConfigReader(path);
        }
        public string BaseUrl
        {
            get
            {
                return _messageProviderConfig["BaseUrl"];
            }
        }

        public string Method
        {
            get
            {
                return _messageProviderConfig["Method"];
            }
        }

        public string Parameters
        {
            get
            {
                return HttpUtility.HtmlDecode(_messageProviderConfig["Parameters"]);
            }
        }

        //public string Password
        //{
        //    get
        //    {
        //        return _messageProviderConfig["Password"];
        //    }
        //}

        public string SuccessResponse
        {
            get
            {
                return _messageProviderConfig["SuccessResponse"];
            }
        }

        //public string User
        //{
        //    get
        //    {
        //        return _messageProviderConfig["User"];
        //    }
        //}

        public bool SendMessage(string No, string Message)
        {
            try
            {
                string Posturl = BaseUrl + "?" + string.Format(Parameters, No, Message);

                WebRequest request = WebRequest.Create(Posturl);
                request.Method = Method;
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();

                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                if (responseFromServer.Contains(SuccessResponse))
                    return true;
                return false;
            }
            catch
            {

                return false;
            }
        }

        public async Task<bool> SendMessageAsync(string No, string Message)
        {
            try
            {
                string Posturl = BaseUrl + "?" + string.Format(Parameters, No, Message);

                WebRequest request = WebRequest.Create(Posturl);
                request.Method = Method;
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                var response = await request.GetResponseAsync();

                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                if (responseFromServer.Contains(SuccessResponse))
                    return true;
                return false;
            }
            catch
            {

                return false;
            }
        }


    }
}
