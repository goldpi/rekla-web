﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Rakla.Infra.Concrete
{
    public sealed class ConfigReader
    {
        public Dictionary<string, dynamic> keyValues = new Dictionary<string, dynamic>();
        public Dictionary<string, dynamic> Properties => keyValues;
        public ConfigReader(string path)
        {
            XDocument doc = XDocument.Load(path);
            var elements = doc.Descendants("add").ToArray();
            foreach (var item in elements)
            {
                string key = item.Attribute("key").Value.ToString();
                dynamic value = item.Attribute("value").Value;
                keyValues.Add(key, value);
            }

        }
        public dynamic this[string index]
        {
            get { return keyValues[index]; }
            set
            {
                if (!keyValues.Keys.Any(i => i == index))
                    keyValues.Add(index, value);
                else
                {
                    keyValues.Remove(index);
                    keyValues.Add(index, value);
                }
            }
        }
    }
}
