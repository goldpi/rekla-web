﻿using Rakla.Infra.Interface;

namespace Rakla.Infra.Concrete
{

    public class ApplicationConfig : IApplicationConfig
    {
        private ConfigReader _reader;
        public ApplicationConfig(string path)
        {
            _reader = new ConfigReader(path);
        }
        public string GetAppProvider(ApplicationProviders type)
        {
            return _reader[type.ToString()];
        }
    }
}