﻿using AutoMapper;
using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel.User
{
   public class UserProfileVM
    {
       
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string Image { get; set; }

        public static ReposetoryMapper<UserProfileVM, UserProfile> VmMapper = 
            new ReposetoryMapper<UserProfileVM, UserProfile>( c => {
            c.CreateMap<string, int>().ConvertUsing(g=>int.Parse(g));
            c.CreateMap<UserProfileVM, UserProfile>();
        }, d => {
            d.CreateMap<int, string>().ConstructUsing(g => g.ToString());
            d.CreateMap<UserProfile, UserProfileVM>();
        });
        public static UserProfileVM ToUserVM(UserProfile profile)
        {
            VmMapper.DataModel = profile;
            return VmMapper.ViewModel;
        }

        public static IEnumerable<UserProfileVM> ToUsersVM(IEnumerable<UserProfile> Profiles)
        {
            foreach (var item in Profiles)
            {
                yield return ToUserVM(item);
            }
        }

        public static UserProfile ToUser(UserProfileVM profile)
        {
            VmMapper.ViewModel = profile;
            return VmMapper.DataModel;
        }

        public static IEnumerable<UserProfile> ToUsers(IEnumerable<UserProfileVM> Profiles)
        {
            foreach (var item in Profiles)
            {
                yield return ToUser(item);
            }
        }
    }
    
}
