﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel.User
{
    public class UserFactory
    {
        IList<UserProfileVM> Profiles;
        public UserFactory()
        {
            List<UserProfileVM> upm = new List<UserProfileVM>();
            upm.Add(new UserProfileVM { Id = "4", Name = "Shahid", UserId = "1", Address = "Near Madina Masjid Hind Piri Ranchi", EmailId = "msshahid563@gmail.com", MobileNo = "8271967193",Image="http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Sam", UserId = "2", Address = "Near Madina Masjid Hind Piri Ranchi", EmailId = "abc@gmail.com", MobileNo = "8271967193" , Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Ram", UserId = "3", Address = "Near Madina Masjid Hind Piri Ranchi", EmailId = "xyz@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Mohan", UserId = "4", Address = "Near Madina Masjid Hind Piri Ranchi", EmailId = "abcd@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Sohan", UserId = "5", Address = "Near Madina Masjid Hind Piri Ranchi", EmailId = "bcd@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Sajid", UserId = "6", Address = "Doranda Ranchi", EmailId = "tyd@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Wajid", UserId = "7", Address = "Doranda Ranchi", EmailId = "hyc@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Sameer", UserId = "8", Address = "Doranda Ranchi", EmailId = "rdc@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Zak", UserId = "9", Address = "Kadru Ranchi", EmailId = "vfc@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Mac", UserId = "10", Address = "Kadru Ranchi", EmailId = "sed@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Bablu", UserId = "11", Address = "Kadru Ranchi", EmailId = "tfd@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Raju", UserId = "12", Address = "Kadru Ranchi", EmailId = "afc@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Madan", UserId = "13", Address = "Lalpur Piri Ranchi", EmailId = "dre@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Mica", UserId = "14", Address = "Lalpur Ranchi", EmailId = "seo@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Imran", UserId = "15", Address = "Lalpur Ranchi", EmailId = "deo@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Yusuf", UserId = "16", Address = "Lalpur Ranchi", EmailId = "xfc@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Shankar", UserId = "17", Address = "Azad Basti Ranchi", EmailId = "gvc@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Dhoni", UserId = "18", Address = "Azad Basti Piri Ranchi", EmailId = "fb@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Shahid", UserId = "19", Address = "Near Madina Masjid Hind Piri Ranchi", EmailId = "ujn@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });
            upm.Add(new UserProfileVM { Id = "4", Name = "Shahid", UserId = "20", Address = "Near Madina Masjid Hind Piri Ranchi", EmailId = "kmj@gmail.com", MobileNo = "8271967193", Image = "http://placehold.it/300x300" });

            Profiles = upm;
        }

        public IList<UserProfileVM> Users => Profiles;
        public void SaveUser(UserProfileVM user)
        { }
        public void DeleteUser(UserProfileVM user)
        { }
    }
}
