﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Data { get; }

        bool Save(TEntity data);
        bool Delete(TEntity data);
        
    }

    
}
