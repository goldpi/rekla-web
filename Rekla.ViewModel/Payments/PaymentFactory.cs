﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel.Payments
{
    public class PaymentFactory
    {
        List<Payment> _Payment = new List<Payment>
            {
                new Payment { Amount=200, Id=Guid.NewGuid().ToString(), IssuedOn=DateTime.Parse("2/14/2017"), PaymentDiscription="Some Random Payment" , PaymentGateUsed="COD", PaymentMode="Cash" , Source="Task of reparing", TaskId=Guid.NewGuid().ToString()},
                new Payment { Amount=200, Id=Guid.NewGuid().ToString(), IssuedOn=DateTime.Parse("2/14/2017"), PaymentDiscription="Some Random Payment" , PaymentGateUsed="COD", PaymentMode="Cash" , Source="Task of reparing", TaskId=Guid.NewGuid().ToString()},
                new Payment { Amount=200, Id=Guid.NewGuid().ToString(), IssuedOn=DateTime.Parse("2/14/2017"), PaymentDiscription="Some Random Payment" , PaymentGateUsed="COD", PaymentMode="Cash" , Source="Task of reparing", TaskId=Guid.NewGuid().ToString()}
            };
        public PaymentFactory()
        {
            
        }
        public void MakePayment(PaymentsViewModel model)
        {
            _Payment.Add(PaymentsViewModel.DataModel(model));
        }
        public IEnumerable<PaymentsViewModel> Payment => PaymentsViewModel.ViewModel(_Payment);

    }
}
