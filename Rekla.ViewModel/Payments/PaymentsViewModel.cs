﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
    public class PaymentsViewModel
    {
        public string Id { get; set; }  
        [Display(Name ="Source Of Payment")]      
        public string Source { get; set; }
        [Display(Name ="Requested Task ID")]
        public string TaskId { get; set; }
        [Display(Name ="Amount")]
        [DisplayFormat(DataFormatString ="Rs. {0}")]
        public decimal Amount { get; set; }
        [Display(Name ="Payment Discription")]
        public string PaymentDiscription { get; set; }
        [Display(Name ="Mode of payment")]
        public string PaymentMode { get; set; }
        [Display(Name = "Payment Gateway")]
        public string PaymentGateUsed { get; set; }
        [Display(Name = "Paid On")]
        [DisplayFormat(DataFormatString ="{0:dd MMM yyyy}")]
        public DateTime IssuedOn { get; set; }

        public static ReposetoryMapper<PaymentsViewModel, Payment> Map = new ReposetoryMapper<PaymentsViewModel, Payment>(c => { c.CreateMap<PaymentsViewModel, Payment>(); }, d => { d.CreateMap<Payment, PaymentsViewModel>(); });
        public static PaymentsViewModel ViewModel(Payment model)
        {
            Map.DataModel = model;
            return Map.ViewModel;
        }
        public static IEnumerable<PaymentsViewModel> ViewModel(IEnumerable<Payment> payments)
        {
            foreach (var item in payments)
            {
                yield return ViewModel(item);
            }
        }
        public static Payment DataModel(PaymentsViewModel model)
        {
            Map.ViewModel = model;
            return Map.DataModel;
        }
        public static IEnumerable<Payment> ViewModel(IEnumerable<PaymentsViewModel> payments)
        {
            foreach (var item in payments)
            {
                yield return DataModel(item);
            }
        }
    }
}
