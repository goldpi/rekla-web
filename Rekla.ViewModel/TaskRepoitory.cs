﻿using Rekla.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rekla.Model;
using System.Linq.Expressions;
using System.Data.Entity;

namespace Rekla.ViewModel
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private DbContext _context;

        public Repository(DbContext Context)
        {
            _context = Context;
        }

        public IQueryable<TEntity> Data
        {
            get
            {
                return _context.Set<TEntity>().AsQueryable<TEntity>();
            }
        }

        public bool Delete(TEntity data)
        {
            _context.Set<TEntity>().Remove(data);
            return _context.SaveChanges() > 0;
        }

        public bool Save(TEntity data)
        {
            _context.Set<TEntity>().Attach(data);
            _context.Entry<TEntity>(data).State = Data.Any(i=>i==data)?EntityState.Modified:EntityState.Added;
            return _context.SaveChanges() > 0;
           
        }
    }
}
