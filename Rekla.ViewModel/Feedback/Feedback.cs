﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
    public class FeedbackViewModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Review { get; set; }
        public DateTime On { get; set; }

        public static ReposetoryMapper<FeedbackViewModel, FeedBack> Mapper = new ReposetoryMapper<FeedbackViewModel, FeedBack>(c => { c.CreateMap<FeedbackViewModel, FeedBack>(); }, d => { d.CreateMap<FeedBack, FeedbackViewModel > (); });
        public static FeedBack Model (FeedbackViewModel dataModel)
        {

            Mapper.ViewModel = dataModel;
            return Mapper.DataModel;
        }
        public static IEnumerable<FeedBack> Model(IEnumerable<FeedbackViewModel> dataModel)
        {

            foreach (var item in dataModel)
            {
               yield return Model(item);
            }
            
        }

        public static FeedbackViewModel DataModel(FeedBack dataModel)
        {

            Mapper.DataModel = dataModel;
            return Mapper.ViewModel;
        }
        public static IEnumerable<FeedbackViewModel> DataModel(IEnumerable<FeedBack> dataModel)
        {

            foreach (var item in dataModel)
            {
                yield return DataModel(item);
            }

        }
    }

    public class FeebackFactory
    {
        private List<FeedBack> _List = new List<FeedBack>
        {
            new FeedBack {  Id="e1", On=DateTime.Now, Review="Saom", UserId="User" },
            new FeedBack {  Id="e2", On=DateTime.Now, Review="Saom", UserId="User" },
            new FeedBack {  Id="e3", On=DateTime.Now, Review="Saom", UserId="User" },
            new FeedBack {  Id="e4", On=DateTime.Now, Review="Saom", UserId="User" },
        };


       public void Add(FeedbackViewModel model)
        {
            _List.Add(FeedbackViewModel.Model(model));
        }

        public IQueryable<FeedbackViewModel> ViewModel => FeedbackViewModel.DataModel(_List).AsQueryable();
    }
}
