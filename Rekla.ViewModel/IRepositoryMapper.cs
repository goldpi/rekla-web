﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
    public interface IRepositoryMapper<TViewModel,TDataModel> where TViewModel :class where TDataModel:class
    {
        
        void ConfigVmToDm(Action<IMapperConfigurationExpression> experssion);
        void ConfigDmToVm(Action<IMapperConfigurationExpression> experssion);
    }

    public class ReposetoryMapper<T, D> : IRepositoryMapper<T, D> where T : class where D : class
    {
        
        Action<IMapperConfigurationExpression> _TtoDconfg;
        Action<IMapperConfigurationExpression> _DtoTconfg;
        public ReposetoryMapper(Action<IMapperConfigurationExpression> TtoD, Action<IMapperConfigurationExpression> DtoTconfg)
        {
           
            _TtoDconfg = TtoD;
            _DtoTconfg = DtoTconfg;
        }
        T _viewModel;
        D _dataModel;
       public  D DataModel
        {
            get
            {
               Mapper.Initialize(_TtoDconfg);
               return Mapper.Map<D>(_viewModel);
            }
            set
            {
                _dataModel = value;
            }
        }
       public T ViewModel
        {
            get
            {
                Mapper.Initialize( _DtoTconfg);
                return Mapper.Map<T>(_dataModel);
            }
            set
            {
                _viewModel = value;
            }
        }
       void IRepositoryMapper<T, D>.ConfigDmToVm(Action<IMapperConfigurationExpression> experssion)
        {
            _DtoTconfg = experssion;
        }
       void IRepositoryMapper<T, D>.ConfigVmToDm(Action<IMapperConfigurationExpression> experssion)
        {
            _TtoDconfg = experssion;
        }
    }
}
