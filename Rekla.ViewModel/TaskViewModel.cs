﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
    public class TaskViewModel
    {
        public string TaskId { get; set; }
        public string TaskTitle { get; set; }
        public string TaskUserId { get; set; }
        public string TaskAddress { get; set; }
        public string TaskByUser { get; set; }
        public DateTime RequestedDateTime { get; set; }
        public bool Completed { get; set; }
    }
}
