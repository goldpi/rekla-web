﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
   public class ServiceTaskListVM
    {
        public string TaskId { get; set; }
        public string UserId { get; set; }
        public string TaskName { get; set; }
        public string Place { get; set; }
        public DateTime OnDate { get; set; }
    }
}
