﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
   public class ComplainVM
    {
        public string TaskId { get; set; }
        public string TaskName { get; set; }
        public string Title { get; set; }
        public string Complain { get; set; }
        public string UserId { get; set; }
        public DateTime ComplainDate { get; set; }
    }
}
