﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Rekla.ViewModel
{
   public class ServicePaymentVM
    {
        public string Id { get; set; }
        [Display(Name = "Source Of Payment")]
        public string Source { get; set; }
        [Display(Name = "Requested Task ID")]
        public string TaskId { get; set; }
        [Display(Name = "Amount")]
        [DisplayFormat(DataFormatString = "Rs. {0}")]
        public decimal Amount { get; set; }
        [Display(Name = "Payment Discription")]
        public string PaymentDiscription { get; set; }
        [Display(Name = "Mode of payment")]
        public string PaymentMode { get; set; }
        [Display(Name = "Payment Gateway")]
        public string PaymentGateUsed { get; set; }
        [Display(Name = "Paid On")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime IssuedOn { get; set; }

    }
}
