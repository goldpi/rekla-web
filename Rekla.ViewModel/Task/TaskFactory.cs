﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel.Task
{
    public class ServiceRequestFactory
    {
        List<RequstedService> _Services = 
            new List<RequstedService> {
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=true},
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=false},
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=true},
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=false},
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=false},
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=false},
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=false},
                        new RequstedService { Address="a", Area="Doranda" , City="Ranchi" , Breif="Locker", MobileNo="999999999", Email="Teste@rean.com", Id= Guid.NewGuid().ToString(), OnDateTime=DateTime.Now, Service="Service",Completed=true},
            };
        public ServiceRequestFactory()
        {

        }

        public IEnumerable<RequstedServiceViewModel> ViewModel => RequstedServiceViewModel.ViewModel(_Services);
        //public IEnumerable<RequstedService> DataModel => RequstedServiceViewModel.DataModel(_Services);

        public void AddService(RequstedServiceViewModel model)
        {
            //Stub Imlementaion
            _Services.Add(RequstedServiceViewModel.DataModel(model));
        }
        public void CompleteTask(string Id)
        {
            var s = _Services.First(i => i.Id == Id);
                s.Completed = true;

        }
    }
}
