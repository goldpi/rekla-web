﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel.Task
{
    public class RequstedServiceViewModel
    {
        public string Id { get; set; }
        [Display(Name = "Service Requested For")]
        public string Service { get; set; }
        [Display(Name = "Service Brief")]
        public string Breif { get; set; }
        [Display(Name = "Area")]
        public string Area { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "Date And Time")]
        [DisplayFormat(DataFormatString = "Date:{0:dd MMM yyyy}, Time:{0:hh:mm tt}")]
        public DateTime OnDateTime { get; set; }
        [Display(Name = "Email Provied")]
        public string Email { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "Contact Number")]
        public string MobileNo { get; set; }
        [Display(Name = "Completed Task")]
        [DisplayFormat(DataFormatString ="{0}")]
        public bool Completed { get; set; }
        public static ReposetoryMapper<RequstedServiceViewModel, RequstedService> Map = new ReposetoryMapper<RequstedServiceViewModel, RequstedService>(c => { c.CreateMap<RequstedServiceViewModel, RequstedService>(); }, d => { d.CreateMap<RequstedService, RequstedServiceViewModel>(); });
        public static RequstedServiceViewModel ViewModel(RequstedService model)
        {
            Map.DataModel = model;
            return Map.ViewModel;
        }
        public static IEnumerable<RequstedServiceViewModel> ViewModel(IEnumerable<RequstedService> payments)
        {
            foreach (var item in payments)
            {
                yield return ViewModel(item);
            }
        }

        public static RequstedService DataModel(RequstedServiceViewModel model)
        {
            Map.ViewModel = model;
            return Map.DataModel;
        }
        public static IEnumerable<RequstedService> DataModel(IEnumerable<RequstedServiceViewModel> payments)
        {
            foreach (var item in payments)
            {
                yield return DataModel(item);
            }
        }
    }
}
