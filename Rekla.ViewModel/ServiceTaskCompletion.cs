﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
   public class ServiceTaskCompletion
    {
        public string TaskId { get; set; }
        public string TaskName { get; set; }
        public string TaskDetails { get; set; }
        public string UserId { get; set; }
        public bool Complete { get; set; }
        public string ByUserId { get; set; }
        public string ByUserName { get; set; }
        public DateTime OnDateTime { get; set; }
    }
}
