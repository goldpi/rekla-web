﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Rekla.ViewModel
{
   public class TaskCancellationVM
    {
        [Key]
        public string TaskId { get; set; }
        public string TaskName { get; set; }
        public string Reason { get; set; } 
        [Display(Name="Reshedule Date")]
        public DateTime RescheduleDate { get; set; }
        [EnumDataType(typeof(TaskCancellationType))]
        public TaskCancellationType Type { get; set; }

    }

    public enum TaskCancellationType
    {
        [Display(Name = "Postponed Task")]
        PostponedTask,
        [Display(Name = "Cancelled Task")]
        CancelledTask

    }
}
