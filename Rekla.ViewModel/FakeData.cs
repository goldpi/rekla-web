﻿using FizzWare.NBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
   public class FakeData
    {
        public IEnumerable<CityModel> Cities()
        {
            return new List<CityModel>
            {
                new CityModel
                {
                     Enabled=true,
                      Id=1.ToString(),
                      Name="Ranchi",
                      ImageUrl="http://placehold.it/350x150?text=Ranchi"

                },
                new CityModel
                {
                     Enabled=true,
                      Id=2.ToString(),
                      Name="Hazaribagh",
                      ImageUrl="http://placehold.it/350x150?text=Hazaribagh"

                },
                new CityModel
                {
                     Enabled=true,
                      Id=3.ToString(),
                      Name="TATA",
                      ImageUrl="http://placehold.it/350x150?text=TATA"

                },
                new CityModel
                {
                     Enabled=true,
                      Id=4.ToString(),
                      Name="Dhanbad",
                      ImageUrl="http://placehold.it/350x150?text=Dhanbad"

                }
            };
        }


        public IEnumerable<Service> Services(string Name)
        {
            //return Builder<Service>.CreateListOfSize(5).All()
            //        .With(i => i.Name = Faker.Name.First()).
            //         With(i => i.ImageUrl = "http://placehold.it/350x150").Build();

            return new List<Service>
            {
                new Service
                {
                    Id=1,
                    Name="Carpenter",
                    ImageUrl="http://placehold.it/350x150"
                },
                new Service
                {
                    Id=2,
                    Name="Electrical",
                    ImageUrl="http://placehold.it/350x150"
                },
                new Service
                {
                    Id=3,
                    Name="Plumber",
                    ImageUrl="http://placehold.it/350x150"
                },
                new Service
                {
                    Id=4,
                    Name="Printing",
                    ImageUrl="http://placehold.it/350x150"
                },
                new Service
                {
                    Id=5,
                    Name="Computer Repair",
                    ImageUrl="http://placehold.it/350x150"
                },
                new Service
                {
                    Id=6,
                    Name="Mechanical",
                    ImageUrl="http://placehold.it/350x150"
                }
            };
        }

        public IEnumerable<Question> Question(string name)
        {
            //var dategenerator = new RandomGenerator();
            //return Builder<Question>.CreateListOfSize(10).All()
            //    .With(i => i.TypeOfQuestion = (TypeOfQuestion)(dategenerator.Next(0, 4)))
            //    .With(i => i.QuestionTitle = Faker.Company.CatchPhrase()).With(i => i.Options = Builder<Option>.CreateListOfSize(dategenerator.Next(4, 7)).All().Build()).Build();
            return new List<Question>
             {
                 new Question
                 {
                     Id=1,
                     QuestionTitle="What is the issue ?",
                     TypeOfQuestion=TypeOfQuestion.MULTILINE,
              
                 },
                 new Question
                 {
                     Id=2,
                     QuestionTitle="Whic type of services do u want ?",
                     TypeOfQuestion=TypeOfQuestion.SINGLESELECT,
                     Options=new List<Option>
                     {
                         new Option
                         {
                             Id=1,
                             AnswerOption="Full repairing",
                             QuestionId=2
                         },
                         new Option
                         {
                             Id=2,
                             AnswerOption="Machine is not working properly",
                             QuestionId=2
                         },
                     }

                 },
                 new Question
                 {
                     Id=3,
                     QuestionTitle="What do u want from us ?",
                     TypeOfQuestion=TypeOfQuestion.MULTISELECT,
                     Options=new List<Option>
                     {
                         new Option
                         {
                             Id=1,
                             AnswerOption="Monthly visiting",
                             QuestionId=3
                         },
                         new Option
                         {
                             Id=2,
                             AnswerOption="Weekly visiting",
                             QuestionId=3
                         },
                         new Option
                         {
                             Id=3,
                             AnswerOption="Quaterly visiting",
                             QuestionId=3
                         },
                         new Option
                         {
                             Id=4,
                             AnswerOption="Yearly visiting",
                             QuestionId=3
                         },
                     }
                 }
             };
        }
    }
}
