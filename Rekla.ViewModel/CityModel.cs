﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.ViewModel
{
    public class CityModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public bool Enabled { get; set; }

    }
    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }

    }
    public class Question
    {
        public int Id { get; set; }
        [EnumDataType(typeof(TypeOfQuestion))]
        public TypeOfQuestion TypeOfQuestion { get; set; }
        public string QuestionTitle { get; set; }

        public ICollection<Option> Options { get; set; }
    }
    public class Option
    {
        public int Id { get; set; }
        public string AnswerOption { get; set; }
        public int QuestionId { get; set; }
    }
    public enum TypeOfQuestion
    {
        MULTILINE,
        SINGLELINE,
        MULTISELECT,
        SINGLESELECT
    }

    public class UserDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public DateTime? Time { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string QuestionerAnswer { get; set; }
    }
}
