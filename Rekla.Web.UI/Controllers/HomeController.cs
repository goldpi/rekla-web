﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;
using Rekla.Model.ContentManagment;

namespace Rekla.Web.UI.Controllers
{
    public class HomeController : Controller
    {
        private ReklaDataContext db = new ReklaDataContext();
        public ActionResult Index()
        {
            return View();
        }

        //[Route("{area}/services")]
        //public ActionResult Search(string area)
        //{
        //    return View();
        //}


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }
        public ActionResult Terms()
        {
            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [Route("Page/{page}")]
        public ActionResult Page(string page)
        {
          if(db.WebPageContents.Any(i=>i.PageName==page))
            return View(db.WebPageContents.Find(page));
          else
          {
              return View(new WebPageContent{  Content= "test"});
          }
        }


        public ActionResult BetaSearch()
        {
            
            return View();
        }

        public ActionResult NG2() => View();
    }
}