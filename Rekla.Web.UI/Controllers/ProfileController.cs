﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;
using System.IO;
using Rakla.Infra.Concrete;

namespace Rekla.Web.UI.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private ReklaDataContext db = new ReklaDataContext();
        private User CurrentUser;
        private ConsumerProfile CurrentProfile;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userName = User.Identity.Name;
            CurrentUser = db.Users.FirstOrDefault(i => i.UserName == userName);
            if (db.Profiles.Any(i => i.UserId == CurrentUser.Id))
            {
                CurrentProfile = db.Profiles.FirstOrDefault(i => i.UserId == CurrentUser.Id);
            }
            else{
                CurrentProfile = new ConsumerProfile
                {
                    MobileNo = CurrentUser.PhoneNumber,
                    EmailId = CurrentUser.Email,
                    Name = CurrentUser.Name,
                    UserId = CurrentUser.Id
                };
                db.Profiles.Add(CurrentProfile);
                db.SaveChanges();
            }
            base.OnActionExecuting(filterContext);
        }

        
        public ActionResult Index()
        {
            ViewBag.User = CurrentUser;
            return View(CurrentProfile);
        }

        public ActionResult Services(int page=0,int size=5)
        {

            var result = db.ServiceRequests.Where(i => i.Email == CurrentUser.Email || i.MobileNo == CurrentUser.PhoneNumber).OrderByDescending(i => i.Date).AsQueryable();
            ViewBag.Pager = Pagers.Items(result.Count()).PerPage(size).Move(page).Segment(5).Center();
            result = result.Skip(page * size).Take(size);
            return PartialView(result.ToList());
        }


        
        // GET: Profile/Edit/5
        public ActionResult Edit()
        {
           
            return View(CurrentProfile);
        }

        // POST: Profile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProfileId,Name,UserId,Address,MobileNo,EmailId,Image")] ConsumerProfile consumerProfile)
        {
            //CurrentProfile = consumerProfile;
            if (ModelState.IsValid)
            {
                db.Entry(CurrentProfile).State = EntityState.Detached;
                db.Entry(consumerProfile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(consumerProfile);
        }

        // GET: Profile/Delete/5
        [HttpPost]
        public ActionResult UpLoadPic(HttpPostedFileWrapper file)
        {
            var path = Server.MapPath("~/content/" + CurrentUser.Id);
            var dir =new DirectoryInfo(path);
            if (file.ContentLength > 0)
            {
                if (dir.Exists)
                    dir.Delete(true);
                dir.Create();
                var fileName = Guid.NewGuid().ToString().Replace('-', '_')+".png";

                file.SaveAs(path + "/" + fileName);
                CurrentProfile.Image = "/content/" + CurrentUser.Id + "/" + fileName;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
