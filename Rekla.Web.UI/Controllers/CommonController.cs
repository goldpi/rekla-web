﻿using Rekla.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Rekla.Web.UI.Controllers
{
    public class CommonController : ApiController
    {
        [HttpGet]
        [Route("api/city")]
        public IEnumerable<CityModel> GetAllCities()
        {
            return new FakeData().Cities();
        }
        [HttpGet]
        [Route("api/service/{name}")]
        public IEnumerable<Service> GetService(string name)
        {
            return new FakeData().Services(name);
        }

        [HttpGet]
        [Route("api/question/{city}/{service}")]
        public IEnumerable<Question> GetQuestion(string city, string service)
        {

            return new FakeData().Question(city);
        }
        [HttpPost]
        [Route("api/submit")]
        public string UserData(UserDetails model)

        {
            return model.Name;
        }

        [HttpPost]
        [Route("api/QuestionAns")]
        public string Question(Question model)

        {
            return model.QuestionTitle;
        }
    }
}
