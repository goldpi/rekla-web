﻿using Rakla.Infra.Interface;
using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Rekla.Web.UI.Controllers
{
    public class PaymentController : Controller
    {
        public PaymentController(ReklaDataContext Context, IGateWay Gateway)
        {
            db = Context;
            Payment = Gateway;
        }
        private IGateWay Payment;
        private Rekla.Model.ReklaDataContext db;

        public void Index(Guid slag)
        {
            var t = db.Transactions.Find(slag);
            var user = db.Users.Find(t.UserId);
            var result = Payment.MakePayment(t.Id.ToString(),
                t.Amount.ToString(),t.Description, user.Email, user.PhoneNumber, user.Name);
            System.Web.HttpContext.Current.Response.Write(result);
            return;
        }

        public ActionResult Success(string slag="")
        {
            if(Request.Form["txnid"]==null && Request.IsLocal)
            {
                return View();
            }
            var TaxId = Guid.Parse(Request.Form["txnid"].ToString());
            var userName = User.Identity.Name;
            var CurrentUser = db.Users.FirstOrDefault(i => i.UserName == userName || i.Email == userName || i.PhoneNumber == userName);
            var tnx = db.Transactions.Find(TaxId);
            var form = Request.Form;
            Dictionary<string, string> d = new Dictionary<string, string>();
            foreach (string i in Request.Form.Keys)
            {
                d.Add(i, Request.Form[i]);
            }
            var save = new JavaScriptSerializer().Serialize(d);
            tnx.TransactionResult = save;
            CurrentUser.Subscription = tnx.Description;
            CurrentUser.IsFree = false;
            db.SaveChanges();
           
            return View(model: save);
        }
        public ActionResult Failed(string slag="")
        {
            var TaxId = Guid.Parse(Request.Form["txnid"].ToString());
            var tnx = db.Transactions.Find(TaxId);
            var form = Request.Form;

            Dictionary<string, string> d = new Dictionary<string, string>();
            foreach (string i in Request.Form.Keys)
            {
                d.Add(i, Request.Form[i]);
            }
            var save = new JavaScriptSerializer().Serialize(d);

            tnx.TransactionResult = save;
            db.SaveChanges();
            return View(model: save);
        }
    }
}