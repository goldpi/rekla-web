﻿using ActionMailer.Net.Mvc;
using Rakla.Infra.Concrete;
using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rekla.Web.UI.Models
{
    public class Emailer:MailerBase
    {

        private ConfigReader Reader;
        public Emailer()
        {
            Reader = new ConfigReader(AppContext.BaseDirectory + "/Config/Email.xml");

        }

        public EmailResult RegMail(ServiceRequest req)
        {

           
            
            this.To.Add(req.Email);
            foreach(var EMAIL in Reader.Properties)
            {
                var email =(string) EMAIL.Value;
                this.To.Add(email);
            }
            //this.To.Add("k.animesh85@gmail.com");
           // this.To.CopyTo(new string[]{ "k.animesh85@gmail.com"},0);
            this.Subject= "Thank you for choosing us as your service partner";
            From = "no-repy@rekla.com";
            return Email("RequestEmail", req);
        }

        public EmailResult ContactUs(string email,string Name,string mobile,string query)
        {
            this.To.Add("k.animesh85@gmail.com");
            this.Subject="Query from Contact";
            From = email;
            ViewBag.Name = Name;
            ViewBag.Mobile = mobile;
            ViewBag.Query = query;
            return Email("ContactUs");
        }
    }
}