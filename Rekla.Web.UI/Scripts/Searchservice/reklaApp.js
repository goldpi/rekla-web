﻿var mainApp = angular.module("mainApp", ['ngRoute', 'oitozero.ngSweetAlert','mgcrea.ngStrap']);
mainApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/scripts/views/main.html',
            controller: 'MainController'
        })
        .when('/:city', {
            templateUrl: '/scripts/views/city.html',
            controller: 'CityController'
        })
        .when('/:city/:service', {
            templateUrl: '/scripts/views/service.html',
            controller: 'ServiceController'
        })
        .when('/:city/:service/q/:no', {
            templateUrl: '/scripts/views/question.html',
            controller: 'QuestionController'
        })
        .when('/:city/:service/q/:no/user', {
            templateUrl: '/scripts/views/userdetail.html',
            controller: 'UserController'
        })

        .otherwise({
            redirectTo: '/'
        });
});