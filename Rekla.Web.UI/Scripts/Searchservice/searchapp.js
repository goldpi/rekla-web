﻿
mainApp.controller("MainController", [
               "$scope","$http", function ($scope,$http) {
                   
        $scope.city = [];
                   $http.get("/api/city/").then(function (response) {
                       $scope.city = response.data;
            console.log(response.data);
        });

    }


]);
mainApp.controller("CityController", [
               "$scope", "$http", "$routeParams", function ($scope, $http, $routeParams) {
                   
                   $scope.service = [];
                   $http.get("/api/service/name").then(function (response) {
            $scope.service = response.data;
            console.log(response.data);
        });
        $scope.city = $routeParams.city;

               }
]);
mainApp.controller("ServiceController", [
    "$scope", "$routeParams", function($scope, $routeParams) {
        $scope.myservice = $routeParams.service;
        $scope.city = $routeParams.city;
        $scope.Questioner = $routeParams.Question;
        $scope.servicelist = "";
    }
]);
mainApp.controller("QuestionController", [
    "$scope", "$http", "$routeParams", "$location", function($scope, $http, $routeParams, $location) {
        $scope.Question = [];
        $scope.index = 0;
        $scope.City = $routeParams.city;

        $scope.Service = $routeParams.service;
        $scope.Form = 0;
        //SelectedQuestion.Options = "";
        //$scope.Answer = "";
        $http.get("/api/question/" + $scope.City + "/" + $scope.Service).then(function(response) {
            $scope.Question = response.data;
            console.log(response.data);
            $scope.SelectedQuestion = $scope.Question[0];
            //if ($scope.SelectedQuestion.TypeOfQuestion==2)
            //    $scope.SelectedQuestion.Answer = Array($scope.SelectedQuestion.Options.length);
           
            //console.log($scope.SelectedQuestion);
        });

        $scope.KeyPress = function (keyEvent) {
            if (keyEvent.which === 13) {
                if ($scope.Form == 1) {
                    save();
                }
                if ($scope.Form == 0) {
                    next();
                }
                
            }
                
        }

        function next() {
            console.log($scope.SelectedQuestion);
            if (($scope.index + 1) < $scope.Question.length) {
                switch ($scope.SelectedQuestion.TypeOfQuestion) {
                    case 0:
                    case 1:
                    case 3:
                        if ($scope.SelectedQuestion.Ans == null) {
                            swal("Error", "Please Provide an answer!", "error");

                            return;
                        }
                        break;
                    case 2:
                        var t = true;
                        for (var i = 0; i < $scope.SelectedQuestion.Options.length; i++) {
                            if ($scope.SelectedQuestion.Options[i].Ans != undefined) {
                                if ($scope.SelectedQuestion.Options[i].Ans === false)
                                    continue;
                                t = false;
                                break;
                            }
                        }
                        if (t) {
                            swal("Error", "Please Provide an answer!", "error");
                            return;
                        }
                        break;
                }



                $scope.index++;
                $scope.SelectedQuestion = $scope.Question[$scope.index];
                //$http.post("/api/QuestionAns/", $scope.SelectedQuestion).then(function(data) {
                //    $scope.SelectedQuestion = data;
                //});

            }
            else {
                $scope.Form = 1;




            }
        }

        $scope.Next = function () {
            next();
        }
        function Question(question,i) {
            switch(question.TypeOfQuestion) {
                case 0:
                case 1:
                case 3:
                    return '\r\n Question '+i+') :' + question.QuestionTitle
                        + '\r\n Answer :' + question.Ans;
                case 2:
               
                    return '\r\n Question ' + i + ') :' + question.QuestionTitle
                        + '\r\n Answer :' + option(question.Options);
            }
        }

        function option(Options) {
            var str = ' ';
            for (i = 0; i < Options.length; i++) {
                if (Options[i].Ans != undefined) {

                    str += (Options[i].Ans == true ? '\r\n Option [' + (i + 1) + ']' + Options[i].AnswerOption : ' ');
                }
            }
            return str;
        }

        function ExtractAnswer() {
            var str = '';
            for (var i = 0; i < $scope.Question.length; i++) {
                str += Question($scope.Question[i],i+1);
            }
            console.log(str);
            return str;
        }
        $scope.phoneNumbr = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;
        $scope.emailerror = /[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
        $scope.Model = {
            Name: null,
            Date: null,
            Time: null,
            City: $scope.City,
            Area: null,
            MobileNo: null,
            Email: null,
            QuestionerAnswer: null
        }
        $scope.Save = function () {
            save();
           
        }


        function save() {
            if ($scope.myForm.$valid) {
                $scope.Model.QuestionerAnswer = ExtractAnswer();

                $http.post("/api/submit", $scope.Model).then(function (data) {

                    $scope.Model = data;
                });

            }
            else {
                swal("Error", "Please fill the form properly!", "error");
            }
        }
    }
]);

mainApp.controller("UserController",["$scope",function($scope) {
    $scope.Model = {
        		Name: null,
        		Date: null,
                Time: null,
                City: $scope.City,
        		Area: null,
        		MobileNo: null,
        		Email: null,
        		Service: null
    }


}])
mainApp.directive('showFocus', function ($timeout) {
    return function (scope, element, attrs) {
        scope.$watch(attrs.showFocus,
            function (newValue) {
                $timeout(function () {
                    newValue && element.focus();
                });
            }, true);
    };
});