﻿using Microsoft.Owin;
using Owin;
using TrackerEnabledDbContext.Common.Configuration;

[assembly: OwinStartupAttribute(typeof(Rekla.Web.UI.Startup))]
namespace Rekla.Web.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            ConfigureAuth(app);
            
        }
    }
}
