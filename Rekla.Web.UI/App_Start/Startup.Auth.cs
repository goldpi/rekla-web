﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using Rekla.Model;
using Rekla.ViewModel;
using Microsoft.Owin.Security.Facebook;

namespace Rekla.Web.UI
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ReklaDataContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, User>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });            
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "163555410920037",
            //   appSecret: "ac0971df342579a1b137a43c57477a6b");

            //test
            //app.UseFacebookAuthentication(
            // appId: "448768685518639",
            // appSecret: "23fea264741b0a01f580f2522f594030");
            var x = new FacebookAuthenticationOptions();
            //x.Scope.Add("email");
            x.AppId = "163555410920037";
            x.AppSecret = "ac0971df342579a1b137a43c57477a6b";
            x.Provider = new FacebookAuthenticationProvider()
            {
                OnAuthenticated = async context =>
                {
                    //Get the access token from FB and store it in the database and
                    //use FacebookC# SDK to get more information about the user
                    context.Identity.AddClaim(new System.Security.Claims.Claim("FacebookAccessToken", context.AccessToken));
                    context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:name", context.Name));
                    context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:email", context.Email));
                    //context.Identity.AddClaim(new System.Security.Claims.Claim("urn:facebook:",context.))
                }
            };
            x.SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie;
            app.UseFacebookAuthentication(x);

            var y = new GoogleOAuth2AuthenticationOptions();
            y.ClientId = "442508879560-43s0sb6f3vu809fbiq5ghfishn7b66vu.apps.googleusercontent.com";
            y.ClientSecret = "fvc_v611i5_u5RwH3hMlozDI";
            y.Scope.Add("email");
            y.Scope.Add("profile");
            app.UseGoogleAuthentication(y);
            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "595547194962-76pd6au5ski4hlvsqlvbsg0sairfuuft.apps.googleusercontent.com",
            //    ClientSecret = "Ly9KhZQNqVYS7xNQSfqBJfZG"
            //});
        }

        //private FacebookAuthenticationOptions GetFacebookAuth()
        //{
        //    string picRequest =
        //        String.Format("/me/picture?redirect=false&width=100&height=100");
        //    var facebookProvider = new FacebookAuthenticationProvider()
        //    {
        //       OnAuthenticated= (context) =>
        //       {
        //           context.
        //       }
        //    }

        //    };
        //    var options = new FacebookAuthenticationOptions()
        //    {
        //        AppId = 0123456789,
        //        AppSecret = ******,
        //        Provider = facebookProvider,
        //    };
        //    return options;
        //}



    }
}