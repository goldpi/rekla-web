﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rekla.ViewModel.User;
namespace Rekla.Web.UI.Areas.Users.Controllers
{
    public class UserProfileController : Controller
    {
        UserFactory Factory = new UserFactory();
        // GET: Users/UserProfile
        public ActionResult Index()
        {  
            return View(Factory.Users.First());
        }
        public ActionResult Edit(string id)
        {  
            if (!string.IsNullOrWhiteSpace(id) )
            {
                return View(Factory.Users.First(i => i.Id == id));
            }
            return View();
        }
        [HttpPost]
        public ActionResult Edit(UserProfileVM model)
        {
            if (ModelState.IsValid)             
            {
                Factory.SaveUser(model);
                return RedirectToAction("Index");
            }
            return View();
        }

        
    }
}