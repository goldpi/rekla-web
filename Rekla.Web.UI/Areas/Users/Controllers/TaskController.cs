﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rekla.Web.UI.Areas.Users.Controllers
{
    public class TaskController : Controller
    {


        // GET: Users/Task
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult TaskCancellationRequest()
        {
            return View();
        }

        public ActionResult TaskCompletion()
        {
            return View();
        }
    }
}