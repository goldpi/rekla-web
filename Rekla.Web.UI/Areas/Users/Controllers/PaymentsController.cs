﻿using Rekla.ViewModel;
using Rekla.ViewModel.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rekla.Web.UI.Areas.Users.Controllers
{
    public class PaymentsController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["Pf"] == null)
                Session["Pf"] = pf = new PaymentFactory();
            else
                pf = (PaymentFactory)Session["Pf"];
            base.OnActionExecuting(filterContext);
        }
        PaymentFactory pf;
        public PaymentsController()
        {
           
        }

        // GET: Users/Payments
        public ActionResult Index()
        {
            return View(pf.Payment);
        }

        public ActionResult Details(string Id)
        {
            return View(pf.Payment.First(i => i.Id == Id));
        }

        public ActionResult MakePayment(string TaskId)
        {
            return View(new PaymentsViewModel
            {
                TaskId = TaskId,
                Amount = 200,
                PaymentDiscription = $"Payment on behalf task {TaskId}",
                Source = "TASK",
                Id = Guid.NewGuid().ToString()

            });
        }
        [HttpPost]
        public ActionResult MakePayment(PaymentsViewModel model)
        {
            if (ModelState.IsValid)
            {
                pf.MakePayment(model);
                Session["Pf"] = pf;
                return RedirectToAction("Index");
            }
            return View(model);
        }
        }
}