﻿using Rekla.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rekla.Web.UI.Areas.Users.Controllers
{
    public class FeedBackController : Controller
    {
        private FeebackFactory _factory;
        
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["feedBack"] != null)
            {
                Session["feedBack"] = _factory = new FeebackFactory();
            }
            else
            {
                _factory = (FeebackFactory)Session["feedBack"];
            }
            base.OnActionExecuting(filterContext);
        }
        // GET: Users/FeedBack
        public ActionResult Index()
        {
            return View(_factory.ViewModel);
        }
    }
}