﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using Rekla.ViewModel;

namespace Rekla.Web.UI.Areas.Users.Controllers
{
    public class FeedbackServiceController : Controller
    {
        // GET: Users/FeedbackService
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Feedback()
        {
            //List<SelectListItem> listItems=new List<SelectListItem>();
            //listItems.Add(new SelectListItem {Text ="Carpenter",Value= "Carpenter" });
            //listItems.Add(new SelectListItem { Text = "Electrician", Value = "Electrician" });
            //listItems.Add(new SelectListItem { Text = "Plumber", Value = "Plumber" });
            //listItems.Insert(0, new SelectListItem { Text = "Select", Value = "Select" });
            //ViewBag.TaskId = new SelectList(listItems, "Text", "Value");
            FeedbackServiceVM model = new FeedbackServiceVM();
            model.TaskName = "Feedback";
            return View(model);
        }
    }
}