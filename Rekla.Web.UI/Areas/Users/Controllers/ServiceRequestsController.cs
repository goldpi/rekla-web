﻿using Rekla.ViewModel.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rekla.Web.UI.Areas.Users.Controllers
{
    public class ServiceRequestsController : Controller
    {
        private ServiceRequestFactory _Factory; 
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Session["Sr"] == null)
            {
                Session["Sr"] = _Factory = new ServiceRequestFactory();
            }
            else
            {
                _Factory = (ServiceRequestFactory)Session["Sr"];
            }
            base.OnActionExecuting(filterContext);
        }
        // GET: Users/ServiceRequests
        public ActionResult Index()
        {
            return View(_Factory.ViewModel);
        }

        public ActionResult Details(string Id)
        {
            return View(_Factory.ViewModel.First(i => i.Id == Id));
        }
        public ActionResult CompleteTask(string Id)
        {
            _Factory.CompleteTask(Id);
            Session["Sr"] = _Factory;
            return RedirectToAction("Index");
        }
        
    }
}