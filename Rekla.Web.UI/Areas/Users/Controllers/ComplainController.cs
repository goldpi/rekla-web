﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rekla.ViewModel;

namespace Rekla.Web.UI.Areas.Users.Controllers
{
    public class ComplainController : Controller
    {
        // GET: Users/Complain
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewComplain()
        {
            ComplainVM model=new ComplainVM();
            model.TaskName = "Complain";
            return View(model);
        }
    }
}