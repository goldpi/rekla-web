﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Rekla.Model;
using Rekla.Web.UI.Models;
using Rakla.Infra.Helper;

namespace Rekla.Web.UI.Controllers
{
    public class ServiceRequestsController : ApiController
    {
        private ReklaDataContext db = new ReklaDataContext();

        // GET: api/ServiceRequests
        public IQueryable<ServiceRequest> GetServiceRequests()
        {
            return db.ServiceRequests;
        }

        // GET: api/ServiceRequests/5
        [ResponseType(typeof(ServiceRequest))]
        public IHttpActionResult GetServiceRequest(int id)
        {
            ServiceRequest serviceRequest = db.ServiceRequests.Find(id);
            if (serviceRequest == null)
            {
                return NotFound();
            }
            if (!string.IsNullOrEmpty(serviceRequest.Email))
            {
                var mailer = new Emailer();
                mailer.RegMail(serviceRequest).Deliver();
            }
            return Ok(serviceRequest);
        }

        // PUT: api/ServiceRequests/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutServiceRequest(int id, ServiceRequest serviceRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != serviceRequest.Id)
            {
                return BadRequest();
            }

            db.Entry(serviceRequest).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiceRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ServiceRequests
        [ResponseType(typeof(ServiceRequest))]
        public IHttpActionResult PostServiceRequest(ServiceRequest serviceRequest)
        {
            serviceRequest.Date = DateTime.Now;
            ModelState.Remove("Date");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ServiceRequests.Add(serviceRequest);
            db.SaveChanges();
            ShortMessageHelper.SendMessage(serviceRequest.MobileNo,$@"Dear {serviceRequest.Name} Thanks for choosing rekla as your service partner. Your service request has been registered with us.Our service partner will contact to you within 30 minutes.");
            if (!string.IsNullOrEmpty(serviceRequest.Email))
            {
                var mailer = new Emailer();
                mailer.RegMail(serviceRequest).Deliver();
            }    
            return CreatedAtRoute("DefaultApi", new { id = serviceRequest.Id }, serviceRequest);
        }

        // DELETE: api/ServiceRequests/5
        [ResponseType(typeof(ServiceRequest))]
        public IHttpActionResult DeleteServiceRequest(int id)
        {
            ServiceRequest serviceRequest = db.ServiceRequests.Find(id);
            if (serviceRequest == null)
            {
                return NotFound();
            }

            db.ServiceRequests.Remove(serviceRequest);
            db.SaveChanges();

            return Ok(serviceRequest);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ServiceRequestExists(int id)
        {
            return db.ServiceRequests.Count(e => e.Id == id) > 0;
        }
    }
}