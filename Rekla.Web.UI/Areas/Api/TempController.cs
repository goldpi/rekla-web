﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Microsoft.Owin;
using Rekla.Web.UI.Models;

namespace Rekla.Web.UI.Controllers
{
    public class TempController : ApiController
    {
        private ReklaDataContext _context = new ReklaDataContext();
        [HttpGet]
        [Route("api/services/List")]
        public IEnumerable<TempService> GetServices()
        {
            var items = _context
                .TempServices
                .Include(i => i.Childs)
                .Include(x => x.Childs.Select(y => y.Childs)).ToList();
            return items;
        }
        [HttpPost]
        [Route("api/ContactUs")]
        public object PostContact([FromBody]dynamic value)
        {
            try
            {
                var name = value.Name.Value;
                var email = value.Email.Value;
                var mobile = value.Mobile.Value;
                var Query = value.Query.Value;
                var mailer = new Emailer();
                mailer.ContactUs(email, name, mobile, Query).Deliver();
                return new { Success = true, Message = "We will get Back to you" };
            }
            catch(Exception ex)
            {
                return new { Success = false, Message = "Something went wrong! Please try again!" };
            }
        }

    }
}
