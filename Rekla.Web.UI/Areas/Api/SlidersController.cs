﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Rekla.Model;

namespace Rekla.Web.UI.Areas.Api
{
    public class SlidersController : ApiController
    {
        private ReklaDataContext db = new ReklaDataContext();

        // GET: api/Sliders
        public IQueryable<Sliders> GetSliders()
        {
            return db.Sliders;
        }

        // GET: api/Sliders/5
        [ResponseType(typeof(Sliders))]
        public IHttpActionResult GetSliders(int id)
        {
            Sliders sliders = db.Sliders.Find(id);
            if (sliders == null)
            {
                return NotFound();
            }

            return Ok(sliders);
        }

        // PUT: api/Sliders/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSliders(int id, Sliders sliders)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sliders.Id)
            {
                return BadRequest();
            }

            db.Entry(sliders).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SlidersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sliders
        [ResponseType(typeof(Sliders))]
        public IHttpActionResult PostSliders(Sliders sliders)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Sliders.Add(sliders);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sliders.Id }, sliders);
        }

        // DELETE: api/Sliders/5
        [ResponseType(typeof(Sliders))]
        public IHttpActionResult DeleteSliders(int id)
        {
            Sliders sliders = db.Sliders.Find(id);
            if (sliders == null)
            {
                return NotFound();
            }

            db.Sliders.Remove(sliders);
            db.SaveChanges();

            return Ok(sliders);
        }

        [Route("api/Sliders/Active")]
        public IQueryable<Sliders> GetActiveSliders()
        {
            return db.Sliders.Where(i=>i.Active==true);
        }


        [Route("api/Services/Active")]
        public IQueryable<Service> Services()
        {
            return db.Services.Where(i => i.Active == true);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SlidersExists(int id)
        {
            return db.Sliders.Count(e => e.Id == id) > 0;
        }
    }
}