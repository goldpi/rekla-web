﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;

namespace Rekla.Web.UI.Areas.Api
{
    public class QuestionerController : ApiController
    {

        private ReklaDataContext dc = new ReklaDataContext();
        // GET: api/Questioner
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Questioner/5
        public ServiceQuestioner Get(int id)
        {
            return dc.ServiceQuestioner.Find(id);
        }

        [ResponseType(typeof(ServiceQuestioner))]
        // POST: api/Questioner
        public IHttpActionResult Post(ServiceQuestioner question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (question.ServiceQuestionerId == 0)
                dc.ServiceQuestioner.Add(question);
            else
                dc.Entry(question).State = System.Data.Entity.EntityState.Modified;
            dc.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = question.ServiceQuestionerId }, question);
        }

        // PUT: api/Questioner/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [HttpGet]
        [Route("api/Questioner/byService/{Id}/{Cid}")]
        public object byServiceId(int Id,int Cid)
        {
            dc.Configuration.LazyLoadingEnabled = false;
            dc.Configuration.ProxyCreationEnabled = false;
            var service = dc.ServiceQuestioner.FirstOrDefault(i => i.ServiceId == Id);
            var ser = dc.Services.Find(Id);
            var city = dc.Cities.Find(Cid);
            if (service == null)
            {
                return new { dataStatus = false, data = service,City=city ,service=ser};
            }
            return new { dataStatus = true, data = service,City=city , service = ser };
        }

        


        // DELETE: api/Questioner/5
        public void Delete(int id)
        {
        }
    }

    public class QuestionVM
    {
        public string value { get; set; }
    }
}
