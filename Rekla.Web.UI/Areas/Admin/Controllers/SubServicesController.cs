﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class SubServicesController : Controller
    {
        private ReklaDataContext db;
        public SubServicesController()
        {
            db =new ReklaDataContext();
            db.ConfigureUsername(() => User.Identity.Name);
        }

        // GET: Admin/SubServices
        public ActionResult Index()
        {
            var subServices = db.TempSubServices.Include(s => s.Parent);
            return View(subServices.ToList());
        }

        // GET: Admin/SubServices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubService subService = db.TempSubServices.Find(id);
            if (subService == null)
            {
                return HttpNotFound();
            }
            return View(subService);
        }

        // GET: Admin/SubServices/Create
        public ActionResult Create()
        {
            ViewBag.ParentId = new SelectList(db.TempServices, "Id", "Name");
            return View();
        }

        // POST: Admin/SubServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ParentId,Terms")] SubService subService)
        {
            if (ModelState.IsValid)
            {
                db.TempSubServices.Add(subService);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ParentId = new SelectList(db.TempServices, "Id", "Name", subService.ParentId);
            return View(subService);
        }

        // GET: Admin/SubServices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubService subService = db.TempSubServices.Find(id);
            if (subService == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentId = new SelectList(db.TempServices, "Id", "Name", subService.ParentId);
            return View(subService);
        }

        // POST: Admin/SubServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ParentId,Terms")] SubService subService)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subService).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ParentId = new SelectList(db.TempServices, "Id", "Name", subService.ParentId);
            return View(subService);
        }

        // GET: Admin/SubServices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubService subService = db.TempSubServices.Find(id);
            if (subService == null)
            {
                return HttpNotFound();
            }
            return View(subService);
        }

        // POST: Admin/SubServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubService subService = db.TempSubServices.Find(id);
            db.TempSubServices.Remove(subService);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
