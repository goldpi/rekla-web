﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;
using Rekla.Model.ContentManagment;
using System.IO;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class CMSController : Controller
    {
        private ReklaDataContext db = new ReklaDataContext();

        private SelectList GetAllTheLayouts(string Selected)
        {
            DirectoryInfo dir = new DirectoryInfo($"{AppDomain.CurrentDomain.BaseDirectory}Views\\Shared\\Layout");
            var List = dir.GetFiles().Select(i => new { Name = i.Name, Value=$"~/Views/Shared/Layout/{i.Name}" });
            return new SelectList(List, "Value", "Name",Selected);
        }
        // GET: Admin/CMS
        public ActionResult Index()
        {
            return View(db.WebPageContents.ToList());
        }

        // GET: Admin/CMS/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebPageContent webPageContent = db.WebPageContents.Find(id);
            if (webPageContent == null)
            {
                return HttpNotFound();
            }
            return View(webPageContent);
        }

        // GET: Admin/CMS/Create
        public ActionResult Create()
        {
            ViewBag.Layout = GetAllTheLayouts("");
            return View(new WebPageContent() {  AddedOn=DateTime.UtcNow,EdidtOn=DateTime.UtcNow, CreatedBy="Admin",EdidtedByUser="admin"});
        }

        // POST: Admin/CMS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "PageName,MetaContent,MetaDiscription,Content,AddedOn,EdidtOn,CreatedBy,EdidtedByUser,Active,Layout")] WebPageContent webPageContent)
        {
            if (ModelState.IsValid)
            {
                db.WebPageContents.Add(webPageContent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Layout = GetAllTheLayouts(webPageContent.Layout);
            return View(webPageContent);
        }

        // GET: Admin/CMS/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebPageContent webPageContent = db.WebPageContents.Find(id);
            if (webPageContent == null)
            {
                return HttpNotFound();
            }
            ViewBag.Layout = GetAllTheLayouts(webPageContent.Layout);
            return View(webPageContent);
        }

        // POST: Admin/CMS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "PageName,MetaContent,MetaDiscription,Content,AddedOn,EdidtOn,CreatedBy,EdidtedByUser,Active,Layout")] WebPageContent webPageContent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(webPageContent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Layout = GetAllTheLayouts(webPageContent.Layout);
            return View(webPageContent);
        }

        // GET: Admin/CMS/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WebPageContent webPageContent = db.WebPageContents.Find(id);
            if (webPageContent == null)
            {
                return HttpNotFound();
            }
            return View(webPageContent);
        }

        // POST: Admin/CMS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            WebPageContent webPageContent = db.WebPageContents.Find(id);
            db.WebPageContents.Remove(webPageContent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
