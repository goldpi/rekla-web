﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ServiceProviderProfilesController : Controller
    {
        private ReklaDataContext db = new ReklaDataContext();

        // GET: Admin/ServiceProviderProfiles
        public ActionResult Index()
        {
            return View(db.ServiceProvider.ToList());
        }

        // GET: Admin/ServiceProviderProfiles/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProviderProfile serviceProviderProfile = db.ServiceProvider.Find(id);
            if (serviceProviderProfile == null)
            {
                return HttpNotFound();
            }
            return View(serviceProviderProfile);
        }

        // GET: Admin/ServiceProviderProfiles/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "ShortName");
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ShortName");
            return View();
        }

        // POST: Admin/ServiceProviderProfiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProfileId,Name,UserId,Address,MobileNo,EmailId,Image,CityId,ServiceId")] ServiceProviderProfile serviceProviderProfile)
        {
            if (ModelState.IsValid)
            {
                db.ServiceProvider.Add(serviceProviderProfile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "ShortName",serviceProviderProfile.CityId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ShortName",serviceProviderProfile.ServiceId);
            return View(serviceProviderProfile);
        }

        // GET: Admin/ServiceProviderProfiles/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProviderProfile serviceProviderProfile = db.ServiceProvider.Find(id);
            if (serviceProviderProfile == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "ShortName", serviceProviderProfile.CityId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ShortName", serviceProviderProfile.ServiceId);
            return View(serviceProviderProfile);
        }

        // POST: Admin/ServiceProviderProfiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProfileId,Name,UserId,Address,MobileNo,EmailId,Image,CityId,ServiceId")] ServiceProviderProfile serviceProviderProfile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceProviderProfile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "ShortName", serviceProviderProfile.CityId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ShortName", serviceProviderProfile.ServiceId);
            return View(serviceProviderProfile);
        }

        // GET: Admin/ServiceProviderProfiles/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceProviderProfile serviceProviderProfile = db.ServiceProvider.Find(id);
            if (serviceProviderProfile == null)
            {
                return HttpNotFound();
            }
            return View(serviceProviderProfile);
        }

        // POST: Admin/ServiceProviderProfiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ServiceProviderProfile serviceProviderProfile = db.ServiceProvider.Find(id);
            db.ServiceProvider.Remove(serviceProviderProfile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
