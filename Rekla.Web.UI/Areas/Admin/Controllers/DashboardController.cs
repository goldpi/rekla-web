﻿using Rekla.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class DashboardController : Controller
    {
        private ReklaDataContext context;

        public DashboardController()
        {
            context = new ReklaDataContext();
        }
        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Aduits(int page=0,int size=20)
        {
            var audits = context.AuditLog.Include(i=>i.LogDetails)
                .OrderByDescending(i=>i.EventDateUTC)
                .Skip(page*size)
                .Take(size).ToList();
            return View(audits);
        }
    }
}