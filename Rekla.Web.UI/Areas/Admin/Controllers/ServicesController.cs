﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;
using System.IO;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ServicesController : Controller
    {
        private ReklaDataContext db = new ReklaDataContext();

        // GET: Admin/Services
        public ActionResult Index()
        {
            return View(db.Services.ToList());
        }

        // GET: Admin/Services/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // GET: Admin/Services/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Services/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "ServiceId,Name,Active,ImageUrl,ShortName,Description,ShowOnService")] Service service)
        {
            if (ModelState.IsValid)
            {
                db.Services.Add(service);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(service);
        }

        // GET: Admin/Services/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Admin/Services/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "ServiceId,Name,Active,ImageUrl,ShortName,Description,ShowOnService")] Service service)
        {
            if (ModelState.IsValid)
            {
                db.Entry(service).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(service);
        }

        // GET: Admin/Services/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // POST: Admin/Services/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Service service = db.Services.Find(id);
            db.Services.Remove(service);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet, ActionName("Questioner")]
        
        public ActionResult Questioner(int id)
        {
            ViewBag.True = db.ServiceQuestioner.Any(i => i.ServiceId == id);
            ViewBag.Id = id;
            if (ViewBag.True == true)
                ViewBag.ServiceQuestionerId = db.ServiceQuestioner.FirstOrDefault(i => i.ServiceId == id).ServiceQuestionerId;
            return View();
        }


        [HttpPost]
        public ActionResult UpLoadPic(HttpPostedFileWrapper file,int Id)
        {
            var service = db.Services.Find(Id);
            var path = Server.MapPath("~/content/Service/" + service.ServiceId);
            var dir = new DirectoryInfo(path);
            if (file.ContentLength > 0)
            {
                if (dir.Exists)
                    dir.Delete(true);
                dir.Create();
                var fileName = Guid.NewGuid().ToString().Replace('-', '_') + ".png";

                file.SaveAs(path + "/" + fileName);
                service.ImageUrl = "/content/Service/" + service.ServiceId + "/" + fileName;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
