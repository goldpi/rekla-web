﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;
using Microsoft.AspNet.Identity.Owin;
using Rekla.Web.UI.Areas.Admin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    //[Authorize(Roles = "admin")]
    public class UsersController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ReklaDataContext db = new ReklaDataContext();
        public UsersController()
        {
        }

        public UsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Admin/Users
        public ActionResult Index(string type="All",int page=0,int size=40)
        {
            var user = db.Users.Include(i => i.Roles);
            switch (type)
            {
                case "admin":
                    var adminRoleId = db.Roles.FirstOrDefault(i => i.Name == "admin").Id;
                    user = user.Where(i => i.Roles.Any(o => o.RoleId == adminRoleId));
                    break;
                case "operator":
                    var operatorRoleId = db.Roles.FirstOrDefault(i => i.Name == "operator").Id;
                    user = user.Where(i => i.Roles.Any(o => o.RoleId == operatorRoleId));
                    break;
            }
            ViewBag.Total = user.Count();
            user = user.OrderBy(i => i.UserName).Skip(page * size).Take(size);
            return View(model:user);
        }

        public ActionResult Roles()
        {
            return View(db.Roles.Select(i=>new Role { Id=i.Id,Name=i.Name }));
        }

        [HttpPost]
        public ActionResult SaveRole(Role model)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            roleManager.Create(new IdentityRole { Name = model.Name });
            return RedirectToAction("Roles");
        }

        // GET: Admin/Users/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Include(i=>i.Roles).FirstOrDefault(u=>u.Id==id);
            ViewBag.Role = db.Roles.ToList();

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        public ActionResult AddRole(string roleName,string userId)
        {
           if(UserManager.IsInRole(userId, roleName))
            {
                UserManager.RemoveFromRole(userId, roleName);
            }
            else
            {
                UserManager.AddToRole(userId, roleName);
            }
            return RedirectToAction("Details", new { id = userId });
        }


        // GET: Admin/Users/Create
        public ActionResult Create()
        {
            ViewBag.Roles = new SelectList(db.Roles, "Name", "Name");
            return View();
        }

        // POST: Admin/Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AdminUserModel user)
        {
            if (ModelState.IsValid)
            {
                var useeri = new User
                {
                    Name = user.Name,
                    Email = user.Email,
                    UserName =!string.IsNullOrEmpty(user.Email)?user.Email: user.PhoneNumber,
                    PhoneNumber = user.PhoneNumber,
                    Referal=User.Identity.IsAuthenticated?User.Identity.Name:"NO USERS"
                };
                var result =UserManager.Create(useeri,user.Password);

                if (result.Succeeded)
                {
                    var re=UserManager.AddToRoles(useeri.Id, user.Roles);
                    if (re.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var e in re.Errors)
                            ModelState.AddModelError("", e);
                    }
                }
                else{
                    foreach (var e in result.Errors)
                        ModelState.AddModelError("", e);
                }
                
            }
            ViewBag.Roles = new MultiSelectList(db.Roles, "Name", "Name",user.Roles);
            return View(user);
        }

       
        // GET: Admin/Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Admin/Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Reffral(string CurrentUser,int page = 0, int size = 30)
        {
            var usersAdded = db.Users.Where(i => i.Referal == CurrentUser).AsQueryable();
            ViewBag.Total = usersAdded.Count();
            usersAdded = usersAdded.OrderBy(i => i.UserName).Skip(page * size).Take(size);
            return View(model: usersAdded.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
