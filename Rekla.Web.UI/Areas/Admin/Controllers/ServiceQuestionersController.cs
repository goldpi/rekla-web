﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ServiceQuestionersController : Controller
    {
        private ReklaDataContext db = new ReklaDataContext();

        // GET: Admin/ServiceQuestioners
        public ActionResult Index()
        {
            var serviceQuestioner = db.ServiceQuestioner.Include(s => s.Service);
            return View(serviceQuestioner.ToList());
        }

        // GET: Admin/ServiceQuestioners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceQuestioner serviceQuestioner = db.ServiceQuestioner.Find(id);
            if (serviceQuestioner == null)
            {
                return HttpNotFound();
            }
            return View(serviceQuestioner);
        }

        // GET: Admin/ServiceQuestioners/Create
        public ActionResult Create()
        {
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name");
            return View();
        }

        // POST: Admin/ServiceQuestioners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServiceQuestionerId,ServiceId,Questioner")] ServiceQuestioner serviceQuestioner)
        {
            if (ModelState.IsValid)
            {
                db.ServiceQuestioner.Add(serviceQuestioner);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name", serviceQuestioner.ServiceId);
            return View(serviceQuestioner);
        }

        // GET: Admin/ServiceQuestioners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceQuestioner serviceQuestioner = db.ServiceQuestioner.Find(id);
            if (serviceQuestioner == null)
            {
                return HttpNotFound();
            }
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name", serviceQuestioner.ServiceId);
            return View(serviceQuestioner);
        }

        // POST: Admin/ServiceQuestioners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServiceQuestionerId,ServiceId,Questioner")] ServiceQuestioner serviceQuestioner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceQuestioner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name", serviceQuestioner.ServiceId);
            return View(serviceQuestioner);
        }

        // GET: Admin/ServiceQuestioners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceQuestioner serviceQuestioner = db.ServiceQuestioner.Find(id);
            if (serviceQuestioner == null)
            {
                return HttpNotFound();
            }
            return View(serviceQuestioner);
        }

        // POST: Admin/ServiceQuestioners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceQuestioner serviceQuestioner = db.ServiceQuestioner.Find(id);
            db.ServiceQuestioner.Remove(serviceQuestioner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
