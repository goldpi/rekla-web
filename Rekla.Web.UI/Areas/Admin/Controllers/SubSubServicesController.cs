﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rekla.Model;

namespace Rekla.Web.UI.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class SubSubServicesController : Controller
    {
        private ReklaDataContext db ;
        public SubSubServicesController()
        {
            db = new ReklaDataContext();
            db.ConfigureUsername(() => User.Identity.Name);
        }

        // GET: Admin/SubSubServices
        public ActionResult Index()
        {
            var subSubServices = db.TempSubSubServices.Include(s => s.Parent);
            return View(subSubServices.OrderBy(i=>i.ParentId).ToList());
        }

        // GET: Admin/SubSubServices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSubService subSubService = db.TempSubSubServices.Find(id);
            if (subSubService == null)
            {
                return HttpNotFound();
            }
            return View(subSubService);
        }

        // GET: Admin/SubSubServices/Create
        public ActionResult Create()
        {
            ViewBag.ParentId = new SelectList(db.TempSubServices.Include(o=>o.Parent).Select(i=>new {Id=i.Id, Name=i.Parent.Name+">"+i.Name }), "Id", "Name");
            return View();
        }

        // POST: Admin/SubSubServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ParentId,Rate")] SubSubService subSubService)
        {
            if (ModelState.IsValid)
            {
                db.TempSubSubServices.Add(subSubService);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ParentId = new SelectList(db.TempSubServices.Include(o => o.Parent).Select(i => new { Id = i.Id, Name = i.Parent.Name + ">" + i.Name }), "Id", "Name", subSubService.ParentId);
            return View(subSubService);
        }

        // GET: Admin/SubSubServices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSubService subSubService = db.TempSubSubServices.Find(id);
            if (subSubService == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentId = new SelectList(db.TempSubServices.Include(o => o.Parent).Select(i => new { Id = i.Id, Name = i.Parent.Name + ">" + i.Name }), "Id", "Name", subSubService.ParentId);
            return View(subSubService);
        }

        // POST: Admin/SubSubServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ParentId,Rate")] SubSubService subSubService)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subSubService).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ParentId = new SelectList(db.TempSubServices.Include(o => o.Parent).Select(i => new { Id = i.Id, Name = i.Parent.Name + ">" + i.Name }), "Id", "Name", subSubService.ParentId);
            return View(subSubService);
        }

        // GET: Admin/SubSubServices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubSubService subSubService = db.TempSubSubServices.Find(id);
            if (subSubService == null)
            {
                return HttpNotFound();
            }
            return View(subSubService);
        }

        // POST: Admin/SubSubServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubSubService subSubService = db.TempSubSubServices.Find(id);
            db.TempSubSubServices.Remove(subSubService);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
