﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rekla.Web.UI.Areas.Admin.Models
{
    public class AdminUserModel
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string[] Roles { get; set; }
    }
}