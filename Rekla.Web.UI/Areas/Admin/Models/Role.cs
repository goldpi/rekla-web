﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rekla.Web.UI.Areas.Admin.Models
{
    public class Role
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}