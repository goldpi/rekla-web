﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rekla.ViewModel;

namespace Rekla.Web.UI.Areas.Provider.Controllers
{
    public class ServiceFeedbackController : Controller
    {
        // GET: Provider/ServiceFeedback
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewFeedback()
        {
            ServiceFeedbackVM model = new ServiceFeedbackVM();
            model.TaskName = "Feedback";
            return View(model);
        }
    }
}