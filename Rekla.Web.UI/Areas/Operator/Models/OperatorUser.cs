﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rekla.Web.UI.Areas.Operator.Models
{
    public class OperatorUser
    {
        public string Name { get; set; }
        [Required]
        [RegularExpression(pattern:@"^\d{10}")]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }

    }
}