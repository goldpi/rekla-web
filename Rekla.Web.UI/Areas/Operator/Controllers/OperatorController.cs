﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Rekla.Model;
using Rekla.Web.UI.Areas.Operator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Rekla.Web.UI.Areas.Operator.Controllers
{
    [Authorize(Roles ="operator,enturn,admin")]
    public class OperatorController : Controller
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ReklaDataContext db = new ReklaDataContext();
        private User CurrentUser;
        public OperatorController()
        {
        }

        public OperatorController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userName = filterContext.HttpContext.User.Identity.Name;
            if (Session["CurrentUser"] != null)
            {
                CurrentUser = (User)Session["CurrentUser"];
            }
            else
            {
                CurrentUser = db.Users.FirstOrDefault(i => i.UserName == userName || i.Email == userName || i.PhoneNumber == userName);
                Session["CurrentUser"] = CurrentUser;
            }


            base.OnActionExecuting(filterContext);
        }

        public ActionResult Index(int page=0,int size=30)
        {
            var usersAdded = db.Users.Where(i => i.Referal == CurrentUser.UserName).OrderBy(i=>i.UserName).Skip(page*size).Take(size).ToList();
            return View(model:usersAdded);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(OperatorUser model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { Email = string.IsNullOrEmpty(model.Email)?model.Email:model.PhoneNumber+"@rekla.in"  , PhoneNumber = model.PhoneNumber, UserName = model.PhoneNumber, Referal=CurrentUser.UserName,Name=model.Name };
                var result=await UserManager.CreateAsync(user, "welcome123");
                if (result.Succeeded)
                {
                    var Profile = new ConsumerProfile
                    {
                        Address = model.Address,
                        MobileNo = model.PhoneNumber,
                        Name = model.Name,
                        ProfileId = Guid.NewGuid().ToString(),
                        UserId = user.Id
                    };
                    db.Profiles.Add(Profile);
                    db.SaveChanges();
                    user.ProfilePicture = UserManager.GeneratePasswordResetToken(user.Id);
                    Rakla.Infra.Helper.ShortMessageHelper.SendMessage(model.PhoneNumber,  "Dear "+ model.Name+" Thanks for subscribing Rekla First. Enjoy our premium services. Your account is valid for 1 year. ");
                    return RedirectToAction("Index");
                }
                foreach(var i in result.Errors)
                {
                    ModelState.AddModelError("", i);
                }
            }
            
            return View(model);
        }

    }
}
