﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rekla.Admin.Startup))]
namespace Rekla.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
