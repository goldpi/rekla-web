#### Application can be divided into following sub Layers

1. **Front End**
2. **Service Man/Team End**
3. **Admin End Front End**


List of modules in **front end**. 

- [ ] 1. Search Module
- [ ] 2. Questioners 
- [ ] 3. Accounts 
- [ ] 4. Payments. 
- [ ] 5. Task Completion  
- [ ] 6. Task Cancelation Request. 
- [ ] 7. History/Status of Service. 
- [ ] 8. History of payments. 
- [ ] 9. Profile 
- [ ] 10.Feedback of service availed. 
- [ ] 11. Complains 


##### Search Module
 
User has to select the area of compliance and 
Select the service provided by the Portal. This module will invoke
Questioners According to the selected fields. 

Working Days [4] 
 
##### Questioners: 
 
User has to answer few questions and fill the form. 
These Questions are to be managed by the Administrator.  
After this module user is prompted either to sign in or sign up to track payments.
  
Working Days [15] 


##### Accounts:
   This is basic Module for users to manage its User profile. Sign In, Change Password, Social Account, etc are included.   
Working Days [10] * 
 With face book and google.com login is include other Account may change the duration and cost. 


##### Payments: 
 Payments have to be made to request for appointments to avail the service
 For that Integration with payment gateway has to be done.
  
Working Days [15]  


##### Task Completion: 
   Confirmation of a task completion has to be approved.
 
Working Days [5] 


##### Task Cancellation Request:

Request for cancelling any future Task to be processed by Administrator. 
And follow ups. 

Working Days [5] 


##### History of Payments:
 
User can track all the payments made by him/her to avail any services.
 
Working Days [5] 


##### History of Service: 

Listing of Histories of all availed services and future task with Person/Team who assigned for the service. 
User can Invoke Feedback or Complain Modules from here for a particular Service. 

Working Days [10] 

##### Feedback of Service availed:

User can give feedback either in rating or as in writing or both.
 
Working Days [5] 


##### Complains: 

User can Invoke complain on a service so that disputes can be resolved.

Working Days [10] 
 
 
 
 
 
Service Man/Team End 
List of modules for Service Man/Team 

- [ ] A) Profile/Account 
- [ ] B) Task Calendar 
- [ ] C) Task Completion Request
- [ ] D) List of Tasks 
- [ ] E) Payments  
- [ ] F) Feed Back of Customer 
- [ ] G) Skills
 
Profile/Account: 
Profile management of Service men, to manage their login and personal details. 
Working Days [10] 
Task Calendar: 
A calendar like Google calendar where all upcoming Task will be marked. User can add remove the his/her/team’s availability for future dates time span etc. 
Working Days [10] 
Task Completion Request: 
Once any task has been complete Service men can reported task as completed. 
Working Days [5] 
List of Task: 
List of all pending, completed and missed task. 
Working Days [5] 
 
Payments: 
Listing of all the payments made by the Portal to the Team/User. 
Working Days [15] 
Feed Back of Customer: 
Adding the feedback of any user who availed their service. 
Working Days [7] 
 
 
Skills: 
Adding removing their Skill sets. 
Working Days [7] 
 Administration  
List of modules in admin. 

- [ ] A) Account/User Management 
- [ ] B) Region Management 
- [ ] C) Service Category Management 
- [ ] D) Questioners Management. 
- [ ] E) Appointments/Task management. 
- [ ] F) Payments  
- [ ] G) CMS 
- [ ] H) Complains 
- [ ] I) Review Feedbacks 
- [ ] J) SMS/Emails 

##### Account/User Management: 

List of all the servicemen/Customers. Add , Block remove like  function will be given here. 
All the service taken/given can be listed from here, and can display all the details of payments made or made by the user. 

Working Days[15]
 
##### Region Management:
 
Add remove edit block Cities/Region/Locality 
Working Days [4] 

##### Service Category Management: 
Add remove edit block any service provided by you in particular region 
Working Days [3] 

##### Questioners Management: 

Add edit the series of questions for a particular service for a region.
 
Working Days [15] 

##### Appointments/Task Management: 

Allotment of the Service Man/Team for a task. Processing of cancellation request.
 Verifying a genuine task. 

Working days [5] 

##### Payments:^ 

List of the transaction made, Making funds transfer and refunds to the consumer.
 
Working days [10]^ 

>^To talks about this.
  
##### CMS: 
Front end Content management system to add static pages and maintain the static contents of those pages. 
>Assigned to Sahid
- [x] 1. Admin->Cms Controller->CRUD
- [x] 2. Ckeditor
- [x] 3. Home->Page(pagename)
>Assigned to Imran
- [x] Image Provider

Working days [5-7] 

##### Complains: 

List of all complains  made by consumers.
  
Working days [3] 
 
##### Review of feedback 

Listing of all the feedbacks either by consumer or service teams 

Working days [3] 

##### SMS/Emails:  

Sending Notification emails and SMS through the panel to the consumers and service providers.With their history 

Working days [3-5] 