#### Intial
> Mockup Start Date: 2 feb 2017 **End Expected** 12 feb 2017

##### Front Section(End) [area / user ]
- [ ] Search Module
- [ ] Questioners
- [ ] Accounts
- [x] Payments.
- [x] Task Completion
- [x] Task Cancelation Request.
- [x] History/Status of Service.
- [x] History of payments.
- [x] Profile
- [x] Feedback of service availed.
- [x] Complains

##### Service Man Section (End)
- [x] Profile/Account
- [ ] Task Calendar
- [x] Task Completion Request
- [x] List of Tasks
- [x] Payments
- [x] Feed Back of Customer
- [ ] Skills

#### Milestone 1

>Date commence 12 Feb 2017

##### Front Section (End)
- [ ] Search
- [ ] Questioners
- [ ] Accounts
- [ ] Payments.
- [ ] Task Creation Only.

##### Admin Section (End)
- [ ] Service Category Management
- [ ] Questioners Management.
- [ ] Appointments/Task management.
- [ ] Payments

#### Milestone 2

##### Front Section (End)
- [ ] Feedback
- [ ] Complains
- [ ] Profile
##### Admin Section (End)
- [ ] Region
- [ ] Account
- [ ] Feedback
- [ ] Complains

#### Milestone 3

##### Front Section (End)
- [ ] Task Cancelation
- [ ] Task Completion
- [ ] History of payments
- [ ] History of service

##### Admin Section (End)
- [ ] Service\Categories Management
- [x] Content Management

##### Service Man Secion (End)
- [ ] Profile/Account
- [ ] Task Calendar

#### Milestone 4
##### Service Man Secion (End)
- [ ] Task Completion Request
- [ ] List of Tasks
- [ ] Payments
- [ ] Feed Back of Customer
- [ ] Skills
- [ ] Testing



