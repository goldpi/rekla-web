﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.Model
{
    public class ServiceRequests
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime Requested { get; set; } = DateTime.UtcNow;
        public string RequestDetails { get; set; } = "['No Details']";
        public bool Completed { get; set; } = false;
        public string AssignedTo { get; set; } = "NOT YET";
        public string MiscDetails { get; set; } = "No Misc";
    }
}
