﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Rekla.Model.ContentManagment;
using Rekla.Model.Subscription;

namespace Rekla.Model
{

    public partial class ReklaDataContext 
    {
        /// <summary>
        /// For static Pages
        /// </summary>
        public DbSet<WebPageContent> WebPageContents { get; set; }
        public DbSet<ServiceRequest> ServiceRequests { get; set; }
        public DbSet<TempService> TempServices { get; set; }
        public DbSet<SubService> TempSubServices { get; set; }
        public DbSet<SubSubService> TempSubSubServices { get; set; }

        public DbSet<City> Cities { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Option> Options { get; set; }

        public DbSet<ServiceProviderProfile> ServiceProvider { get; set; }
        public DbSet<ServiceQuestioner> ServiceQuestioner { get; set; }
        public DbSet<Skills> Skills { get; set; }
        public DbSet<SubArea> SubAreas { get; set; }


        public DbSet<Sliders> Sliders { get; set; }
        public DbSet<ConsumerProfile> Profiles { get; set; }
        public DbSet<Package> Packages { get; set; }

        public DbSet<Transaction> Transactions { get; set; }
    }
}