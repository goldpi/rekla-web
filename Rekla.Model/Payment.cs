﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{

    public class Transaction
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        //public TransactionsType Type { get; set; } = TransactionsType.Subscription;
        public double Amount { get; set; }
        public string Description { get; set; }
        
       // public bool IsForCart => CartId.HasValue;
        public string UserId { get; set; }

        public string TransactionResult { get; set; }
        public DateTime On { get; set; } = DateTime.UtcNow;

    }
    public class Payment
    {
        [Key]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Source { get; set; }
        public string TaskId { get; set; }
        public decimal Amount { get; set; }
        public string PaymentDiscription { get; set; }
        public string PaymentMode { get; set; }
        public string PaymentGateUsed { get; set; }
        public DateTime IssuedOn { get; set; }

    }
}
