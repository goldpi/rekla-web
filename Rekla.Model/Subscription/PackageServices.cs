﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rekla.Model.Subscription
{

   
    public class PackageServices
    {
        [Key,Column(Order =1)]
        public int PackageId { get; set; }
        [Key,Column(Order =2)]
        public int ServiceId { get; set; }
        public int Quantity { get; set; }
        public virtual Package Package { get; set; }
        public virtual Service Service { get; set; }
    }
   
    
}
