﻿using System.Collections.Generic;

namespace Rekla.Model.Subscription
{
    public class Package
    {
        public int Id { get; set; }
        public string PackageName { get; set; }
        public string Details { get; set; }
        public int ValidityInDays { get; set; }
        public decimal Price { get; set; }
        public bool ShowOnSignUp { get; set; }
        public ICollection<PackageServices> PackageServices { get; set; }

    }    
}
