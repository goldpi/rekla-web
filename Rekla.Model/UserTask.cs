﻿using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class UserTask
    {
        [Key]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ToUserId { get; set; }
        public string RequstedServiceId { get; set; }

    }
}
