﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class RequstedService
    {
        [Key]
        public string Id { get; set; }
        public string Service { get; set; }
        public string Breif { get; set; }
        public string Area { get; set; }
        public string City { get; set; }
        public DateTime OnDateTime { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public bool Completed { get; set; }

    }
}
