﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class Skills
    {
        [Key]
        public string SkillId { get; set; } = Guid.NewGuid().ToString();
        public string SkillName { get; set; }
        public virtual ICollection<ServiceProviderProfile> Providers { get; set; }

    }
}
