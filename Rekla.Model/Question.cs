﻿using System.Collections.Generic;

namespace Rekla.Model
{
    public class Question
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public int QuestionerId { get; set; }
        public virtual Questioner Questioner { get; set; }
        public virtual ICollection<Option> Options { get; set; }
    }
}
