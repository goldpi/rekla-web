﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.Model
{
    public class Questioner
    {
        public int QuestionerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
    }
}
