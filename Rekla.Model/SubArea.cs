﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class SubArea
    {
        [Key]
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string PINCODE { get; set; }
        public int CityId { get; set; }
        public bool Active { get; set; }
        
    }
}
