﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.Model
{
    [TrackChanges]
    public class City
    {
        public int CityId { get; set; }
        public string Name { get; set; } = "City Name";
        public string ShortName { get; set; }
        public bool Active { get; set; } = false;
        //public string UserId { get; set; } = "Admin";
        //public DateTime AddedOn { get; set; } = DateTime.UtcNow;
        //public DateTime EditOn { get; set; } = DateTime.UtcNow;
        public string ImageUrl { get; set; }
        public virtual ICollection<Service> Services { get; set; }

    }
}
