﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class FeedBack
    {
        [Key]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Review { get; set; }
        public DateTime On { get; set; }
    }
}
