﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using TrackerEnabledDbContext.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rekla.Model
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class User : IdentityUser
    {
        public string Name { get; set; }
        public string ProfilePicture { get; set; }
        public string Subscription { get; set; }
        public bool IsFree { get; set; }
        public DateTime? Expiry { get; set; }

        public string Referal { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public partial class ReklaDataContext : TrackerIdentityContext<User>
    {
        //public ReklaDataContext()
        //    :base.base("DefaultConnection", throwIfV1Schema: false)
        //{
        //}
        public ReklaDataContext():base("DefaultConnection")
        {
           
        }
        public static ReklaDataContext Create()
        {
            return new ReklaDataContext();
        }

        
    }
    [TrackChanges]
    [Table("TempService")]
    public class TempService
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public virtual ICollection<SubService> Childs { get; set; }
    }
    [TrackChanges]
    [Table("TempSubServices")]
    public class SubService
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public string Terms { get; set; }
        public virtual TempService Parent { get; set; }
        public virtual ICollection<SubSubService> Childs { get; set; }
    }
    [TrackChanges]
    [Table("TempSubSubServices")]
    public class SubSubService
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public virtual SubService Parent { get; set; }
        public string Rate { get; set; }


    }
    [TrackChanges]
    [Table("TempServiceRequests")]
    public class ServiceRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        [Required, Phone]
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Service { get; set; }

    }
}