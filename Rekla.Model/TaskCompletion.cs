﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class TaskCompletion
    {
        [Key]
        public string Id { get; set; }
        public string TaskId { get; set; }
        public bool Completed { get; set; }
        public DateTime On { get; set; }
    }
}
