﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class Appointment
    {
        [Key]
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string UserId { get; set; }
        public string ToUserId { get; set; }
        public string AppointmentsDeatils { get; set; }
        public DateTime On { get; set; }
    }
}
