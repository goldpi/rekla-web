﻿namespace Rekla.Model
{
    public enum TypeOfOption
    {
        Radio,
        CheckBox,
        TextBox
    }
}
