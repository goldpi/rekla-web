﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class ConsumerProfile
    {
        [Key]
        public string ProfileId { get; set; }= Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string UserId { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string Image { get; set; }
    }

   
    
    
}
