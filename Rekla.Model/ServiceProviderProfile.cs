﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class ServiceProviderProfile
    {
        [Key]
        public string ProfileId { get; set; } = Guid.NewGuid().ToString();
        public int CityId { get; set; }
        public int ServiceId { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string Address { get; set; }
        public string MobileNo { get; set; }
        public virtual ICollection<Skills> Skills { get; set; }
        public string EmailId { get; set; }
        public string Image { get; set; }
        public virtual City City { get; set; }
        public virtual Service Service { get; set; }
    }
}
