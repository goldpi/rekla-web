﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.Model.ContentManagment
{
    public class WebPageContent
    {
        [Key]
        public string PageName { get; set; }
        public string MetaContent { get; set; }
        public string MetaDiscription { get; set; }
        public string Layout { get; set; }
        public string Content { get; set; }
        public DateTime AddedOn { get; set; }
        public DateTime EdidtOn { get; set; }
        public string CreatedBy { get; set; }
        public string EdidtedByUser { get; set; }
        public bool Active { get; set; }
    }
}
