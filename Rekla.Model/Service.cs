﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    [TrackChanges]
    public class Service
    {
        public int ServiceId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; } = false;
        public bool ShowOnService { get; set; } = false;
        //public string UserId { get; set; } = "Admin";
        public string ImageUrl { get; set; }
        //public DateTime AddedOn { get; set; } = DateTime.UtcNow;
        //public DateTime EditOn { get; set; } = DateTime.UtcNow;
        //public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<City> Cities { get; set; }
        //public int CityId { get; set; }
        //public virtual City City { get; set; }
    }
}
