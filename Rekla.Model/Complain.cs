﻿using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class Complain
    {
        [Key]
        public string Id { get; set; }
        public string ByUserId { get; set; }
        public string ToUserId { get; set; }
        public string Matter { get; set; }
    }
}
