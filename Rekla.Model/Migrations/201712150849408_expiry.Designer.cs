// <auto-generated />
namespace Rekla.Model.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class expiry : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(expiry));
        
        string IMigrationMetadata.Id
        {
            get { return "201712150849408_expiry"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
