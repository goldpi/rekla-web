namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class te : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Packages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PackageName = c.String(),
                        Details = c.String(),
                        ValidityInDays = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ShowOnSignUp = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PackageServices",
                c => new
                    {
                        PackageId = c.Int(nullable: false),
                        ServiceId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PackageId, t.ServiceId })
                .ForeignKey("dbo.Packages", t => t.PackageId, cascadeDelete: true)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.PackageId)
                .Index(t => t.ServiceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PackageServices", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.PackageServices", "PackageId", "dbo.Packages");
            DropIndex("dbo.PackageServices", new[] { "ServiceId" });
            DropIndex("dbo.PackageServices", new[] { "PackageId" });
            DropTable("dbo.PackageServices");
            DropTable("dbo.Packages");
        }
    }
}
