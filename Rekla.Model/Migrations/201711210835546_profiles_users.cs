namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class profiles_users : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConsumerProfiles",
                c => new
                    {
                        ProfileId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        UserId = c.String(),
                        Address = c.String(),
                        MobileNo = c.String(),
                        EmailId = c.String(),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.ProfileId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ConsumerProfiles");
        }
    }
}
