namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reffral_added : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Referal", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Referal");
        }
    }
}
