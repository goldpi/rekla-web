namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class user_subs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Subscription", c => c.String());
            AddColumn("dbo.AspNetUsers", "IsFree", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsFree");
            DropColumn("dbo.AspNetUsers", "Subscription");
        }
    }
}
