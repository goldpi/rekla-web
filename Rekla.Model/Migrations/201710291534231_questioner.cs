namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class questioner : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Questioners",
                c => new
                    {
                        QuestionerId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.QuestionerId);
            
            AddColumn("dbo.Questions", "QuestionerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Questions", "QuestionerId");
            AddForeignKey("dbo.Questions", "QuestionerId", "dbo.Questioners", "QuestionerId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questions", "QuestionerId", "dbo.Questioners");
            DropIndex("dbo.Questions", new[] { "QuestionerId" });
            DropColumn("dbo.Questions", "QuestionerId");
            DropTable("dbo.Questioners");
        }
    }
}
