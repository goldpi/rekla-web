namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class serviceQuestioner : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ServiceQuestioners", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Questions", "ServiceQuestioner_ServiceQuestionerId", "dbo.ServiceQuestioners");
            DropIndex("dbo.Questions", new[] { "ServiceQuestioner_ServiceQuestionerId" });
            DropIndex("dbo.ServiceQuestioners", new[] { "CityId" });
            AddColumn("dbo.ServiceQuestioners", "Questioner", c => c.String());
            DropColumn("dbo.Questions", "ServiceQuestioner_ServiceQuestionerId");
            DropColumn("dbo.ServiceQuestioners", "CityId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ServiceQuestioners", "CityId", c => c.Int(nullable: false));
            AddColumn("dbo.Questions", "ServiceQuestioner_ServiceQuestionerId", c => c.Int());
            DropColumn("dbo.ServiceQuestioners", "Questioner");
            CreateIndex("dbo.ServiceQuestioners", "CityId");
            CreateIndex("dbo.Questions", "ServiceQuestioner_ServiceQuestionerId");
            AddForeignKey("dbo.Questions", "ServiceQuestioner_ServiceQuestionerId", "dbo.ServiceQuestioners", "ServiceQuestionerId");
            AddForeignKey("dbo.ServiceQuestioners", "CityId", "dbo.Cities", "CityId", cascadeDelete: true);
        }
    }
}
