namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServiceShow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "ShowOnService", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Services", "ShowOnService");
        }
    }
}
