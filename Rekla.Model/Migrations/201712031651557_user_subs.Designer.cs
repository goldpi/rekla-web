// <auto-generated />
namespace Rekla.Model.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class user_subs : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(user_subs));
        
        string IMigrationMetadata.Id
        {
            get { return "201712031651557_user_subs"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
