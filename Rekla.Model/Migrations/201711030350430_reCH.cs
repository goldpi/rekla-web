namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reCH : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Services", "City_CityId", "dbo.Cities");
            DropIndex("dbo.Services", new[] { "City_CityId" });
            CreateTable(
                "dbo.ServiceCities",
                c => new
                    {
                        Service_ServiceId = c.Int(nullable: false),
                        City_CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Service_ServiceId, t.City_CityId })
                .ForeignKey("dbo.Services", t => t.Service_ServiceId, cascadeDelete: false)
                .ForeignKey("dbo.Cities", t => t.City_CityId, cascadeDelete: false)
                .Index(t => t.Service_ServiceId)
                .Index(t => t.City_CityId);
            
            DropColumn("dbo.Services", "City_CityId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Services", "City_CityId", c => c.Int());
            DropForeignKey("dbo.ServiceCities", "City_CityId", "dbo.Cities");
            DropForeignKey("dbo.ServiceCities", "Service_ServiceId", "dbo.Services");
            DropIndex("dbo.ServiceCities", new[] { "City_CityId" });
            DropIndex("dbo.ServiceCities", new[] { "Service_ServiceId" });
            DropTable("dbo.ServiceCities");
            CreateIndex("dbo.Services", "City_CityId");
            AddForeignKey("dbo.Services", "City_CityId", "dbo.Cities", "CityId");
        }
    }
}
