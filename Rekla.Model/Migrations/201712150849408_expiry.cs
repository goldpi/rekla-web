namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class expiry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Expiry", c => c.DateTime(nullable:true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Expiry");
        }
    }
}
