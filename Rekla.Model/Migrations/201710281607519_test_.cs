namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test_ : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Options",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Title = c.String(),
                        GroupWith = c.String(),
                        HasNextQuestion = c.Boolean(nullable: false),
                        NextQuestionId = c.Long(nullable: false),
                        Question_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.Question_Id)
                .Index(t => t.Question_Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(),
                        ServiceQuestioner_ServiceQuestionerId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ServiceQuestioners", t => t.ServiceQuestioner_ServiceQuestionerId)
                .Index(t => t.ServiceQuestioner_ServiceQuestionerId);
            
            CreateTable(
                "dbo.ServiceProviderProfiles",
                c => new
                    {
                        ProfileId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        UserId = c.String(),
                        Address = c.String(),
                        MobileNo = c.String(),
                        EmailId = c.String(),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.ProfileId);
            
            CreateTable(
                "dbo.Skills",
                c => new
                    {
                        SkillId = c.String(nullable: false, maxLength: 128),
                        SkillName = c.String(),
                    })
                .PrimaryKey(t => t.SkillId);
            
            CreateTable(
                "dbo.ServiceQuestioners",
                c => new
                    {
                        ServiceQuestionerId = c.Int(nullable: false, identity: true),
                        CityId = c.Int(nullable: false),
                        ServiceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceQuestionerId)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.CityId)
                .Index(t => t.ServiceId);
            
            CreateTable(
                "dbo.SubAreas",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        PINCODE = c.String(),
                        CityId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SkillsServiceProviderProfiles",
                c => new
                    {
                        Skills_SkillId = c.String(nullable: false, maxLength: 128),
                        ServiceProviderProfile_ProfileId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Skills_SkillId, t.ServiceProviderProfile_ProfileId })
                .ForeignKey("dbo.Skills", t => t.Skills_SkillId, cascadeDelete: true)
                .ForeignKey("dbo.ServiceProviderProfiles", t => t.ServiceProviderProfile_ProfileId, cascadeDelete: true)
                .Index(t => t.Skills_SkillId)
                .Index(t => t.ServiceProviderProfile_ProfileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceQuestioners", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.Questions", "ServiceQuestioner_ServiceQuestionerId", "dbo.ServiceQuestioners");
            DropForeignKey("dbo.ServiceQuestioners", "CityId", "dbo.Cities");
            DropForeignKey("dbo.SkillsServiceProviderProfiles", "ServiceProviderProfile_ProfileId", "dbo.ServiceProviderProfiles");
            DropForeignKey("dbo.SkillsServiceProviderProfiles", "Skills_SkillId", "dbo.Skills");
            DropForeignKey("dbo.Options", "Question_Id", "dbo.Questions");
            DropIndex("dbo.SkillsServiceProviderProfiles", new[] { "ServiceProviderProfile_ProfileId" });
            DropIndex("dbo.SkillsServiceProviderProfiles", new[] { "Skills_SkillId" });
            DropIndex("dbo.ServiceQuestioners", new[] { "ServiceId" });
            DropIndex("dbo.ServiceQuestioners", new[] { "CityId" });
            DropIndex("dbo.Questions", new[] { "ServiceQuestioner_ServiceQuestionerId" });
            DropIndex("dbo.Options", new[] { "Question_Id" });
            DropTable("dbo.SkillsServiceProviderProfiles");
            DropTable("dbo.SubAreas");
            DropTable("dbo.ServiceQuestioners");
            DropTable("dbo.Skills");
            DropTable("dbo.ServiceProviderProfiles");
            DropTable("dbo.Questions");
            DropTable("dbo.Options");
        }
    }
}
