namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class serviceChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ServiceCities", "Service_ServiceId", "dbo.Services");
            DropForeignKey("dbo.ServiceCities", "City_CityId", "dbo.Cities");
            DropForeignKey("dbo.Services", "Service_ServiceId", "dbo.Services");
            DropIndex("dbo.Services", new[] { "Service_ServiceId" });
            DropIndex("dbo.ServiceCities", new[] { "Service_ServiceId" });
            DropIndex("dbo.ServiceCities", new[] { "City_CityId" });
            AddColumn("dbo.Services", "Description", c => c.String());
            AddColumn("dbo.Services", "City_CityId", c => c.Int());
            CreateIndex("dbo.Services", "City_CityId");
            AddForeignKey("dbo.Services", "City_CityId", "dbo.Cities", "CityId");
            DropColumn("dbo.Services", "Service_ServiceId");
            DropTable("dbo.ServiceCities");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ServiceCities",
                c => new
                    {
                        Service_ServiceId = c.Int(nullable: false),
                        City_CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Service_ServiceId, t.City_CityId });
            
            AddColumn("dbo.Services", "Service_ServiceId", c => c.Int());
            DropForeignKey("dbo.Services", "City_CityId", "dbo.Cities");
            DropIndex("dbo.Services", new[] { "City_CityId" });
            DropColumn("dbo.Services", "City_CityId");
            DropColumn("dbo.Services", "Description");
            CreateIndex("dbo.ServiceCities", "City_CityId");
            CreateIndex("dbo.ServiceCities", "Service_ServiceId");
            CreateIndex("dbo.Services", "Service_ServiceId");
            AddForeignKey("dbo.Services", "Service_ServiceId", "dbo.Services", "ServiceId");
            AddForeignKey("dbo.ServiceCities", "City_CityId", "dbo.Cities", "CityId", cascadeDelete: true);
            AddForeignKey("dbo.ServiceCities", "Service_ServiceId", "dbo.Services", "ServiceId", cascadeDelete: true);
        }
    }
}
