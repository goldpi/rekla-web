namespace Rekla.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class servicePro : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cities", "ShortName", c => c.String());
            AddColumn("dbo.Services", "ShortName", c => c.String());
            AddColumn("dbo.ServiceProviderProfiles", "CityId", c => c.Int(nullable: false));
            AddColumn("dbo.ServiceProviderProfiles", "ServiceId", c => c.Int(nullable: false));
            CreateIndex("dbo.ServiceProviderProfiles", "CityId");
            CreateIndex("dbo.ServiceProviderProfiles", "ServiceId");
            AddForeignKey("dbo.ServiceProviderProfiles", "CityId", "dbo.Cities", "CityId", cascadeDelete: true);
            AddForeignKey("dbo.ServiceProviderProfiles", "ServiceId", "dbo.Services", "ServiceId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceProviderProfiles", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.ServiceProviderProfiles", "CityId", "dbo.Cities");
            DropIndex("dbo.ServiceProviderProfiles", new[] { "ServiceId" });
            DropIndex("dbo.ServiceProviderProfiles", new[] { "CityId" });
            DropColumn("dbo.ServiceProviderProfiles", "ServiceId");
            DropColumn("dbo.ServiceProviderProfiles", "CityId");
            DropColumn("dbo.Services", "ShortName");
            DropColumn("dbo.Cities", "ShortName");
        }
    }
}
