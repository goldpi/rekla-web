﻿using System.Collections.Generic;

namespace Rekla.Model
{
    public class ServiceQuestioner
    {
        public int ServiceQuestionerId { get; set; }
       // public int CityId { get; set; }
        public int ServiceId { get; set; }
       // public virtual City City { get; set; }
        public virtual Service Service { get; set; }
        public string Questioner { get; set; }
                                           // public virtual ICollection<Question> Questions { get; set; }
    }
}
