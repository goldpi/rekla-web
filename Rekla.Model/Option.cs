﻿using System.ComponentModel.DataAnnotations;

namespace Rekla.Model
{
    public class Option
    {
        public long Id { get; set; }
        [EnumDataType(typeof(TypeOfOption))]
        public TypeOfOption Type { get; set; }
        public string Title { get; set; }
        public string GroupWith { get; set; }
        public bool HasNextQuestion { get; set; }
        public long NextQuestionId { get; set; }

    }
}
