﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekla.Model
{
    public class Sliders
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string ImageUrl { get; set; }
        public bool Active { get; set; }
    }
}
